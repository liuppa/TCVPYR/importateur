package fr.univpau.liuppa.tcvpyr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ProcessBuilder.Redirect;
import java.net.URI;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;
import fr.univpau.liuppa.tcvpyr.reader.IReader;
import fr.univpau.liuppa.tcvpyr.reader.ger.GerXmlReader;
import fr.univpau.liuppa.tcvpyr.reader.ren.RenXmlReader;
import fr.univpau.liuppa.tcvpyr.writer.elastic.ElasticWriter;

/**
 * Class defining the interfaces to interact with the component 
 * 'Importateur' of the 'Application Federatrice'.
 * 
 * @author André Fonteles
 *
 */
public class Importateur {

	private static final String CP_IMG_REN = "cp-img-ren.sh";
	private static final String CP_IMG_GER = "cp-img-ger.sh";
	
	private static final String IMAGES_DEST_PATH = "/opt/tomcat/webapps/ROOT/images/";
	private static final String ELASTIC_INDEX = "objetetude";
	
	private static final String IMAGES_DEST_PATH_DEV = "/opt/tomcat/webapps/GeoInventaire-dev/images/";
	private static final String ELASTIC_INDEX_DEV = "objetetude-dev";
	
	private String imgDestPath;
	private String elasticIndex;
		
	private IReader reader;
	private ElasticWriter elasticWriter;
	
	private String imgOriPath;
	private String script;
	
	public enum ImportOrigin {
		RENABL_LP2,
		GERTRUDE
	}
	
	/**
	 * Instantiate a new Importateur to read files in a path and write to
	 * a Elasticsearch database.
	 * 
	 * @param importSource - Original application used to export the data being imported
	 * @param path - Path containing the files with information to be imported
	 * @param urlElasticsearch - String with a URL to the Elasticsearch database.
	 * @param production - Boolean defining whether data should be imported to production or developer application
	 * 
	 * @throws ParserConfigurationException 
	 */
	public Importateur(ImportOrigin importSource, String path, String urlElasticsearch, boolean production) throws ParserConfigurationException {
		this.imgOriPath = path;
		
		if(production) {
			imgDestPath = IMAGES_DEST_PATH;
			elasticIndex = ELASTIC_INDEX;
		} else {	
			imgDestPath = IMAGES_DEST_PATH_DEV;
			elasticIndex = ELASTIC_INDEX_DEV;
		}
		
		switch(importSource) {
		case GERTRUDE:
			reader = new GerXmlReader(path);
			script = CP_IMG_GER;
			break;
		case RENABL_LP2:
			reader = new RenXmlReader(path);
			script = CP_IMG_REN;
			break;				
		}
		
		elasticWriter = new ElasticWriter(urlElasticsearch, elasticIndex);
	}
	
	public void importData() {
		List<ObjetEtude> oeList = reader.read();
		
		System.out.println("[" + new Date() + "] Importateur: " + oeList.size() + " 'ObjetEtude' read from path.");
		
		elasticWriter.write(oeList);
		
		// If the the current operating system is linux, execute shell script
		// to copy images
		if(System.getProperty("os.name").equalsIgnoreCase("Linux")) {
			copyImages();
		} else {
			System.out.println("[" + new Date() + "] Warning! Images should be copied manually.");
		}
	}
	
	/**
	 * Copy images from 'path' to 'IMAGES_PATH'.
	 */
	private void copyImages() {
		try {
			URI uri = Importateur.class.getProtectionDomain().getCodeSource().getLocation().toURI();
			String jarPath = new File(uri).getParent();
			
			File scriptFile = new File(jarPath + '/' + script);
			
			if(!scriptFile.exists()) {
				System.out.println("[" + new Date() + "] Importateur: " + script + " does not yet exist");
				String internalPath = "assets/" + script;
				extractFile(internalPath, scriptFile);
				System.out.println("[" + new Date() + "] Importateur: " + script + " extracted from jar");
			}

			// Call bash script to copy images from 'path' to 'IMAGES_PATH'
	        ProcessBuilder pb = new ProcessBuilder("/bin/sh", scriptFile.getPath(), imgOriPath, imgDestPath);
	        pb.redirectOutput(Redirect.INHERIT);
	        pb.redirectError(Redirect.INHERIT);
	        Process p = pb.start();
	        p.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Extract a file from the program JAR file to the same directory 
	 * where the JAR is.
	 * 
	 * @param internalPath - path to the internal file to be extracted from jar
	 * @param destinationFile - file to where the internal file will be extracted
	 * @throws IOException
	 */
	private static void extractFile(String internalPath, File destinationFile) throws IOException {
		InputStream is = GerXmlReader.class.getClassLoader().getResourceAsStream(internalPath);
		byte[] buffer = new byte[is.available()];
		
		is.read(buffer);
		
		FileOutputStream fileOutputStream = new FileOutputStream(destinationFile);
		fileOutputStream.write(buffer);
		
		fileOutputStream.flush();
		fileOutputStream.close();
	}
}
