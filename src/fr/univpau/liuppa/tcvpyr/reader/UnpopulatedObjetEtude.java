package fr.univpau.liuppa.tcvpyr.reader;

import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;

/**
 * Class representing any ObjetEtude not loaded from the
 * ElasticSearch. Typically, an instance of this class
 * will have nothing more than a reference and maybe
 * some liens.
 * 
 * If the complete information about this ObjetEtude is 
 * needed, one should query ElasticSearch using its reference.
 * 
 * @author André Fonteles
 */
public class UnpopulatedObjetEtude extends ObjetEtude {
	
	public UnpopulatedObjetEtude(String reference) {
		setReference(reference);
	}
	
}
