package fr.univpau.liuppa.tcvpyr.reader;

import java.util.List;

import fr.univpau.liuppa.tcvpyr.model.Lien;
import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;

/**
 * Set of Utility classes to help in the reading of a {@link Lien}
 * 
 * @author André Fonteles
 */
public class LienUtils {

	/**
	 * Check whether the {@link Lien} newLien has been already
	 * set and configured within two {@link ObjetEtude}
	 * 
	 * @param newLien - Lien to be tested
	 * @return true if newLien is already set
	 */
	public static boolean lienAlreadySet(Lien newLien) {
		List<Lien> liens = newLien.getDestination().getLiens();

		String newDestRef;
		String newSourceRef;
		String oldDestRef;
		String oldSourceRef;
		
		for (Lien oldLien : liens) {
			newDestRef = newLien.getDestination().getReference();
			newSourceRef = newLien.getSource().getReference();
			oldDestRef = oldLien.getDestination().getReference();
			oldSourceRef = oldLien.getSource().getReference();

			if(newDestRef.equals(oldDestRef) && newSourceRef.equals(oldSourceRef) && newLien.getType().equals(oldLien.getType()))
				return true;
		}
		
		return false;
	}
	
}
