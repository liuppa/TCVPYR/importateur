/**
 * Provides one or more classes related to reading data from a XML 
 * exported by RenablLP 2 and transforming it into the project data model.
 */
package fr.univpau.liuppa.tcvpyr.reader.ren;