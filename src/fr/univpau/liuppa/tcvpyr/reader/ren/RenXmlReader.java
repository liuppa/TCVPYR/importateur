package fr.univpau.liuppa.tcvpyr.reader.ren;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

import fr.univpau.liuppa.tcvpyr.model.Adresse;
import fr.univpau.liuppa.tcvpyr.model.Illustration;
import fr.univpau.liuppa.tcvpyr.model.Lien;
import fr.univpau.liuppa.tcvpyr.model.Merimee;
import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;
import fr.univpau.liuppa.tcvpyr.model.Palissy;
import fr.univpau.liuppa.tcvpyr.model.Personne;
import fr.univpau.liuppa.tcvpyr.model.Protection;
import fr.univpau.liuppa.tcvpyr.model.Role;
import fr.univpau.liuppa.tcvpyr.reader.DateUtils;
import fr.univpau.liuppa.tcvpyr.reader.IReader;

/**
 * Class responsible for reading all the XML files
 * exported by RenablLP 2 contained in a same folder.
 * 
 * @author André Fonteles
 */
public class RenXmlReader implements IReader {

	private final DocumentBuilderFactory factory;
	private final DocumentBuilder builder;
	private final String rootPath;

	private List<Merimee> merimeList = null;
	private List<Palissy> palissyList = null;
	private List<Illustration> illustrationsList = null;
	
	private LienRenSet lienRenSet;
	
	private GeometryFactory geometryFactory;
	
	public RenXmlReader(String rootDir) throws ParserConfigurationException {
		factory = DocumentBuilderFactory.newInstance();
		builder = factory.newDocumentBuilder();
		this.rootPath = rootDir;

		merimeList = new ArrayList<Merimee>();
		palissyList = new ArrayList<Palissy>();
		illustrationsList = new ArrayList<Illustration>();
		
		lienRenSet = new LienRenSet();
		
		geometryFactory = new GeometryFactory();
	}

	/* (non-Javadoc)
	 * @see fr.univpau.iutbayonne.tcvpyr.reader.elastic.IReader#read()
	 */
	public List<ObjetEtude> read() {
		File rootDir = new File(rootPath);
		File[] listOfFiles = rootDir.listFiles();
		
		System.out.println("[" + new java.util.Date() + "] RenXmlReader: parsing " + rootDir.getAbsolutePath());
		
		for (File file : listOfFiles) {
			// Get file extension
			String fileExt = file.getName().substring(file.getName().length()-3, file.getName().length());			
			
			if (file.isFile() && fileExt.equalsIgnoreCase("xml")) {
				try {
					System.out.println("Parsing: " + file.getName());
					Document document = builder.parse(file);
					Element root = document.getDocumentElement();
					parseElement(root);
				} catch (IOException | SAXException e) {
					e.printStackTrace();
				}
			}
		}

		// Process the set of LienRen previously extracted from the XML
		Set<String> sources = lienRenSet.getSourceSet();
		for (String sourceRef : sources) {
			ObjetEtude objetSource = findObjetEtude(sourceRef);
			for(LienRen lienRen : lienRenSet.getLiensRenBySource(sourceRef)) {
				// If lienRen is linking the source to an illustration, attach illustration to ObjetEtude source.
				if(lienRen.getType().equalsIgnoreCase(LienRen.ILLUSTRATION_SIGNIFICATIVE) || lienRen.getType().equalsIgnoreCase(LienRen.ILLUSTRATION)) {
					Illustration i = findIllustration(lienRen.getDestinationRef());
					objetSource.addIllustration(i);
					
					if(lienRen.getType().equalsIgnoreCase(LienRen.ILLUSTRATION_SIGNIFICATIVE)) {
						i.setSignificative(true);
					}
				} else { // If lienRen is not related to illustration, create a Lien between source and destination
					ObjetEtude objetDestination = findObjetEtude(lienRen.getDestinationRef());
					if(objetDestination != null) {
						Lien lien = new Lien(objetSource, objetDestination, lienRen.getType());
						objetDestination.addLien(lien);
						objetDestination.addLienDe(lien);
						
						objetSource.addLien(lien);
						objetSource.addLienVers(lien);
					}
				}
			}
		}
		
		ArrayList<ObjetEtude> oeList = new ArrayList<ObjetEtude>();
		oeList.addAll(merimeList);
		oeList.addAll(palissyList);
		return oeList;
	}

	/**
	 * Extract a Merimee, Palissy or Illustration and its links (LienRen) from Element root
	 * 
	 * @param root - XML element containing the notice RenablLP 2
	 */
	private void parseElement(Element root) {
		String newNoticeRef = "";
		
		try {
			Element eNotice = (Element) root.getElementsByTagName("MERIMEE").item(0);
			if (eNotice != null) { // It eNotice represents a Merimee
				Merimee newMerimee = parseMerimee(eNotice);
				merimeList.add(newMerimee);
				newNoticeRef = newMerimee.getReference();
			} else { // If not Merimee, try Palissy
				eNotice = (Element) root.getElementsByTagName("PALISSY").item(0);
				if (eNotice != null) {
					Palissy newPalissy = parsePalissy(eNotice);
					palissyList.add(newPalissy);
					newNoticeRef = newPalissy.getReference();
				} else { // If neither Merimee nor Palissy, try Illustration
					eNotice = (Element) root.getElementsByTagName("ILLUSTRATION").item(0);
					if (eNotice != null) {
						Illustration newIllustration = parseIllustration(eNotice);
						illustrationsList.add(newIllustration);
						newNoticeRef = newIllustration.getReference();
					} else { // If not Merimee nor Palissy nor Illustration, throw exception
						throw new RuntimeException("The XML does not contain a Merimee, nor a Palissy, nor an Illustration.");
					}
				}
			}
			
			// Read and create all LiensRen that have newNotice as source
			parseLiensRen(root, newNoticeRef);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Populate the fields of objetEtude with information 
	 * from an XML element of a notice of type Merimee or Palissy
	 * 
	 * @param eNotice - XML element from which objetEtude will be populated
	 * @param objetEtude - {@link ObjetEtude} to be populated from eNotice
	 */
	private void parseMerimeePalissyNotice(Element eNotice, ObjetEtude objetEtude) {
		// reference
		objetEtude.setReference(eNotice.getAttribute("REF"));

		// titre
		Element eTico = (Element) eNotice.getElementsByTagName("TICO").item(0);
		objetEtude.setTitre(eTico.getTextContent());

		// description
		Element eDesc = (Element) eNotice.getElementsByTagName("DESC").item(0);
		objetEtude.setDescription(eDesc.getTextContent());

		// historique
		Element eHist = (Element) eNotice.getElementsByTagName("HIST").item(0);
		objetEtude.setHistorique(eHist.getTextContent());

		// type
		Element eType = (Element) eNotice.getElementsByTagName("DENO").item(0);
		objetEtude.setType(eType.getTextContent());
		
		// anneeConstructions
		Element eDateC = (Element) eNotice.getElementsByTagName("DATE").item(0);
		String dateC = eDateC.getTextContent();
		if(dateC.length() != 0) {
			String dates[] = dateC.split(";");
			for (String annee : dates) {
				objetEtude.addAnneesConstructions(Integer.parseInt(annee.trim()));
			}
		}
		
		// anneeConstructions
		Element eSCLE = (Element) eNotice.getElementsByTagName("SCLE").item(0);
		
		Element eSCLD = (Element) eNotice.getElementsByTagName("SCLD").item(0);
		
		String scle = eSCLE.getTextContent();
		String scld = "";
		if(eSCLD != null)
			scld = eSCLD.getTextContent();
		
		String scls = "";
		
		if(scle.length() > 0) {
			scls += scle;
		}
		if(scle.length() > 0 && scld.length() > 0) {
			scls += " ; ";
		}
		if(scld.length() > 0) {
			scls += scld;
		}
		
		String datations[] = scls.split(";");
		for (String datation : datations) {
			objetEtude.addDatation(datation.trim());
		}
		
		// dateSaisie
		Element eDateS = (Element) eNotice.getElementsByTagName("DBOR").item(0);
		String dateS = eDateS.getTextContent();
		dateS = dateS.split(";")[0];
		if(dateS.length() > 0) {
			objetEtude.setDateSaisie(DateUtils.parse(dateS));
		}
		
		// adresse
		parseAdresse(eNotice, objetEtude);
		
		// protection
		parseProtections(eNotice, objetEtude);
		
		// Chercheurs
		parseChercheurs(eNotice, objetEtude);
		
		// rolePersonnes
		parseRoles(eNotice, objetEtude);
	}
	
	/**
	 * Extract a list of {@link Role} from eNotice and set it to objetEtude
	 * 
	 * @param eNotice - XML element from which objetEtude will be populated
	 * @param objetEtude - {@link ObjetEtude} to be populated from eNotice
	 */
	private void parseRoles(Element eNotice, ObjetEtude objetEtude) {
		// Extract role/personne from AUTR and PERS
		Element eAutr = (Element) eNotice.getElementsByTagName("AUTR").item(0);
		Element ePers = (Element) eNotice.getElementsByTagName("PERS").item(0);
		
		String autrPers = eAutr.getTextContent() + ";" + ePers.getTextContent();
		String rolesString[] = autrPers.split(";+(?![^\\(]*\\))");
		
		for (String fullNameAndRole : rolesString) {
			fullNameAndRole = fullNameAndRole.trim();
			
			if(fullNameAndRole.length() > 0) {
				objetEtude.addRolesPersonnes(extractRoles(fullNameAndRole));
			}
		}
	}

	/**
	 * Extract and return a list of {@link Role} from a String containing 
	 * a full-name and one or more roles (e.g., "Spont Simon, docteur (commanditaire)", 
	 * "Bernard Bertrand (peintre ; décorateur)").
	 * 
	 * @param fullNameAndRole - String containing the raw list o names and roles
	 * @return a list of roles extracted from the String
	 */
	private List<Role> extractRoles(String fullNameAndRole) {
		ArrayList<Role> roles = new ArrayList<Role>();
		Role role;
		
		int iOpeningP;
		int iClosingP;
		String fullName;
		String rawDescription;
		
		iOpeningP = fullNameAndRole.indexOf('(');
		iClosingP = fullNameAndRole.indexOf(')');
		
		fullName = fullNameAndRole.substring(0, iOpeningP).trim();
		rawDescription = fullNameAndRole.substring(iOpeningP+1, iClosingP).trim();
		
		String descriptions[] = rawDescription.split(";");
		Personne personne = extractPerssone(fullName);
		
		for (String desc : descriptions) {
			role = new Role();
			role.setDescription(desc.trim());
			role.setPersonne(personne);
			roles.add(role);
		}

		return roles;
	}
	
	/**
	 * Extract all the "chercheurs" {@link Personne} from eNotice and set it to 
	 * objetEtude.
	 * 
	 * @param eNotice - XML element from which objetEtude will be populated
	 * @param objetEtude - {@link ObjetEtude} to be populated from eNotice
	 */
	private void parseChercheurs(Element eNotice, ObjetEtude objetEtude) {
		/* 
		 * Extract natureProtection and dateProtection from NOMS (multivalued field)
		 * 
		 * Examples of NOMS values:
		 * - "Mercier Marie ; Dupont Norbert" 
		 * - "Calastrenc Carine"
		 */
		Element eNoms = (Element) eNotice.getElementsByTagName("NOMS").item(0);
		
		// Get the last entry only
		String noms[] = eNoms.getTextContent().split(";");
		
		for (String nom : noms) {
			objetEtude.addChercheur(extractPerssone(nom.trim()));
		}
	}

	/**
	 * Extract and return a {@link Personne} from a 
	 * full-name string such as "Isaac Newton" or "la Taille Alice de".
	 * 
	 * @param fullName - String containing a person's full name
	 * @return personne containing the first and last name extracted from the String
	 */
	private Personne extractPerssone(String fullName) {
		int iCapLetter = -1;
		
		// Divide prenom and nom from the first white space after a capital leter. 
		// It Avoids problems with full names such as "la Taille Alice de"
		do {
			iCapLetter++;
		} while (iCapLetter < fullName.length() && !Character.isUpperCase(fullName.charAt(iCapLetter)));
		
		if(iCapLetter==fullName.length())
			iCapLetter = 0;
		
		int iSpace = fullName.indexOf(' ', iCapLetter);
		
		String nom = "";
		String prenom = "";
		
		if(iSpace > 0) {
			nom = fullName.substring(0, iSpace);
			prenom = fullName.substring(iSpace + 1);
		} else { // If only one name is provided, consider it to be the last name.
			nom = fullName;
		}
		return new Personne(prenom, nom);
	}

	/**
	 * Extract all protections({@link Protection}) from eNotice and set 
	 * it to objetEtude
	 * 
	 * @param eNotice - XML element from which objetEtude will be populated
	 * @param objetEtude - {@link ObjetEtude} to be populated from eNotice
	 */
	private void parseProtections(Element eNotice, ObjetEtude objetEtude) {
		/* 
		 * Extract natureProtection and dateProtection from DPRO (multivalued field)
		 * 
		 * Examples of DPRO values:
		 * - "1907/08/23 : classé au titre objet" 
		 * - "1840 : classé au titre immeuble"
		 * - "1921/06/08 ; 1928/05/02 : classé MH"
		 */
		Element eNatProt = (Element) eNotice.getElementsByTagName("DPRO").item(0);
		
		// Split all the protections into an array
		String dpros[] = eNatProt.getTextContent().split(";");
		
		for (String dpro : dpros) {
			int iColon = dpro.indexOf(':');
			if(iColon > 0) { // If a colon exists, then data concerning protection must be extracted
				
				String dateProt = dpro.substring(0, iColon);
				String natureProt = dpro.substring(iColon+2).trim();
				
				int iSemicolon = natureProt.indexOf(';');
				if(iSemicolon > 0)
					natureProt = natureProt.substring(0, iSemicolon).trim();
				
				Protection protection = new Protection();
				protection.setNature(natureProt);
				protection.setDate(DateUtils.parse(dateProt));
				
				objetEtude.addProtection(protection);
			}
		}
	}

	/**
	 * Extract an {@link Adresse} from eNotice and set it to objetEtude
	 * 
	 * @param eNotice - XML element from which objetEtude will be populated
	 * @param objetEtude - {@link ObjetEtude} to be populated from eNotice
	 */
	private void parseAdresse(Element eNotice, ObjetEtude objetEtude) {
		// Municipalite
		Adresse adresse = new Adresse();
		Element eCom = (Element) eNotice.getElementsByTagName("COM").item(0);
		adresse.setMunicipalite(eCom.getTextContent());

		// Région
		Element eReg = (Element) eNotice.getElementsByTagName("REG").item(0);
		adresse.setRegion(eReg.getTextContent());

		/* 
		 * Extract the road name, type of the road and street number from 
		 * ADRS (a multivalued field).
		 * 
		 * Examples of ADRS values:
		 * - "Ludovic Dardenne (boulevard) 2" 
		 * - "Charles Tron (boulevard) 6 ; Sylvie (rue)"
		 * - "Quinconces (Cours des)"
		 */
		Element eAdrs = (Element) eNotice.getElementsByTagName("ADRS").item(0);
		String adrs = eAdrs.getTextContent();

		if(adrs != null) {
			// We take only the first address
			adrs = adrs.split(";")[0];
			
			if(adrs.length() > 0) {
				int iOpeningP = adrs.indexOf('(');
				int iClosingP = adrs.indexOf(')');
		
				String nomVoie = adrs.substring(0, iOpeningP).trim();
				String typeVoie = adrs.substring(iOpeningP+1, iClosingP).trim();
				String numero = (iClosingP+1<adrs.length()) ? adrs.substring(iClosingP+1).trim() : "";
				adresse.setNomVoie(nomVoie);
				adresse.setTypeVoie(typeVoie);
				adresse.setNumero(numero);
			}
		}
		
		// Geographical Coordinates
		Element eCoorWgs84 = (Element) eNotice.getElementsByTagName("COORWGS84").item(0);
		String coordinates = eCoorWgs84.getTextContent();
		if(coordinates.length() > 0) {
			double latitude = Double.parseDouble(eCoorWgs84.getTextContent().split(",")[0].trim());
			double longitude = Double.parseDouble(eCoorWgs84.getTextContent().split(",")[1].trim());
			Point point = geometryFactory.createPoint(new Coordinate(longitude, latitude));
			adresse.setLocalisation(point);
		} else {
			Element eCoorMWgs84 = (Element) eNotice.getElementsByTagName("COORWGS84").item(0);
			if(eCoorMWgs84 != null && !eCoorMWgs84.getTextContent().isEmpty()) // We currently do not support multiple points location with Renabl.
				throw new RuntimeException("Skiping notice: " + objetEtude.getReference() + " REASON : unsupported geometry");
			else
				throw new RuntimeException("Skiping notice: " + objetEtude.getReference() + " REASON : No coordinates found");
		}
		
		objetEtude.setAdresse(adresse);
	}

	/**
	 * Create a Merimee object and populate its fields with 
	 * information from an XML element of a notice of type Merimee
	 * 
	 * @param eNotice - XML element containing an ObjetEtude Merimee
	 * @return merimee extracted from eNotice
	 */
	private Merimee parseMerimee(Element eNotice) {
		Merimee merimee = new Merimee();
		parseMerimeePalissyNotice(eNotice, merimee);

		return merimee;
	}

	/**
	 * Create a Palissy object and populate its fields with 
	 * information from an XML element of a notice of type Palissy
	 * 
	 * @param eNotice - XML element containing an ObjetEtude Palissy
	 * @return merimee extracted from eNotice
	 */
	private Palissy parsePalissy(Element eNotice) {
		Palissy palissy = new Palissy();
		parseMerimeePalissyNotice(eNotice, palissy);

		return palissy;
	}

	/**
	 * Create an Illustration object and populate its fields
	 * with information from an XML Element of a notice of type Illustration
	 * 
	 * @param eNotice - XML element containing an Illustration
	 * @return illustration extracted from eNotice
	 */
	private Illustration parseIllustration(Element eNotice) {
		Illustration illustration = new Illustration();

		// Get reference
		illustration.setReference(eNotice.getAttribute("REF"));
		illustration.setReference(eNotice.getAttribute("NUMI"));

		// Get accesMiniature
		Element eAccesMiniature = (Element) eNotice.getElementsByTagName("FNU1").item(0);
		illustration.setAccesMiniature(eAccesMiniature.getTextContent());

		// Get accesImage
		Element eAccesImage = (Element) eNotice.getElementsByTagName("FNU2").item(0);
		illustration.setAccesImage(eAccesImage.getTextContent());
		
		// Get accesImage
		Element eLegende = (Element) eNotice.getElementsByTagName("LEG").item(0);
		illustration.setLegende(eLegende.getTextContent());

		return illustration;
	}

	/**
	 * Read and create a list of {@link LienRen} having sourceRef as 
	 * source according to the XML element root. All liensRen created 
	 * are inserted into object liensRenSet.
	 * 
	 * @param root - root xml element
	 * @param sourceRef - reference that must be in the source of the {@link LienRen}
	 */
	private void parseLiensRen(Element root, String sourceRef) {
		Element eLiens = (Element)root.getElementsByTagName("LIENS").item(0);
		NodeList liensNodeList = eLiens.getChildNodes();
		Element eLien = null;
		LienRen lienR = null;
		
		// Iterate over all liens ("LIEN_SOURCE")
		for (int i = 0; i < liensNodeList.getLength(); i++) {
			Node node = liensNodeList.item(i);
			
			// If there is a link (i.e., an element "LIEN_SOURCE")
			if(node != null && node.getNodeName().equals("LIEN_SOURCE")) {
				eLien = (Element)node;
				Element eType = (Element) eLien.getElementsByTagName("TYP").item(0);
				Element eDestination = (Element) eLien.getElementsByTagName("DESTINATION").item(0);
				
				lienR = new LienRen();
				lienR.setSourceRef(sourceRef);
				String destinationRef = null;
				
				// If type is defined
				if(eType != null) {
					lienR.setType(eType.getTextContent());
				}
				
				Element eDestinationRef = (Element)eDestination.getElementsByTagName("REF").item(0);
				// If eDestinationRef is defined
				if(eDestinationRef != null) {
					destinationRef = eDestinationRef.getTextContent();
					lienR.setDestinationRef(destinationRef);
				}
				
				lienRenSet.addLienRenabl(lienR);
			}
		}
	}

	
	/**
	 * Search for a Merimee or Palissy with reference equals to the reference
	 * passed as parameter
	 * 
	 * @param reference - reference of the {@link ObjetEtude} to be found
	 * @return objetEtude - objetEtude containing the same reference or <code>null</code> if none exists
	 */
	private ObjetEtude findObjetEtude(String reference) {
		for (Merimee merimee : merimeList) {
			if(merimee.getReference().equals(reference)) {
				return merimee;
			}
		}
		for (Palissy palissy : palissyList) {
			if(palissy.getReference().equals(reference)) {
				return palissy;
			}
		}
		return null;
	}
	
	/**
	 * Search for an Illustration with reference equals to the reference
	 * passed as parameter
	 * 
	 * @param reference - reference of the {@link Illustration} to be found
	 * @return illustration containing the same reference or <code>null</code> if none exists
	 */
	private Illustration findIllustration(String reference) {
		for (Illustration illustration : illustrationsList) {
			if(illustration.getReference().equals(reference)) {
				return illustration;
			}
		}
		return null;
	}
}
