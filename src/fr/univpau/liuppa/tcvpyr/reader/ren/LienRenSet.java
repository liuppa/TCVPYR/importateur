package fr.univpau.liuppa.tcvpyr.reader.ren;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Utility class used to create a set (with no repeated elements, by definition)
 * of links ({@link LienRen}).
 * 
 * @author André Fonteles
 */
public class LienRenSet {

	private HashMap<String, HashMap<String, LienRen>> mapOfMaps;

	public LienRenSet() {
		mapOfMaps = new HashMap<String, HashMap<String, LienRen>>();
	}
	
	/**
	 * Add {@link LienRen} lienRen, if it does not exist, to the set of links.
	 * 
	 * @param lienRen - lienRen to added
	 */
	public void addLienRenabl(LienRen lienRen) {
		HashMap<String, LienRen> liensRenBySource = mapOfMaps.get(lienRen.getSourceRef());
		
		// Check if the set of liens with the same source already exists
		if(liensRenBySource == null) {
			liensRenBySource = new HashMap<String, LienRen>();
			mapOfMaps.put(lienRen.getSourceRef(), liensRenBySource);
		}
		
		// Add the lien to the set of liens with the same source
		liensRenBySource.put(lienRen.getDestinationRef(), lienRen);
	}
	
	/**
	 * Retrieve a collection containing the subset of {@link LienRen} that
	 * have String sourceRef as the source.
	 * 
	 * @param sourceRef - reference to be in the source
	 * @return subset containing sourceRef as source
	 */
	public Collection<LienRen> getLiensRenBySource(String sourceRef) {
		HashMap<String, LienRen> liensRenBySource = mapOfMaps.get(sourceRef);
		
		if(liensRenBySource != null) {
			return liensRenBySource.values();
		}
		
		return null;
	}

	/**
	 * Retrieve a {@link LienRen} by its source's and destination's references.
	 * 
	 * @param sourceRef - reference to be in the source
	 * @param destinationRef - reference to be in the destination
	 * @return lienRen having the same source and destination
	 */
	public LienRen getLienRenabl(String sourceRef, String destinationRef) {
		HashMap<String, LienRen> liensRenBySource = mapOfMaps.get(sourceRef);
		LienRen lienRenabl = null;
		
		if(liensRenBySource != null) {
			lienRenabl = liensRenBySource.get(destinationRef);
		}
		
		return lienRenabl;
	}
	
	/**
	 * Retrieve a list containing all elements {@link LienRen} of this set.
	 * 
	 * @return list of LienRen
	 */
	public List<LienRen> values() {
		ArrayList<LienRen> allLiens = new ArrayList<LienRen>();
		
		for (HashMap<String, LienRen> liensByDestinationSource : mapOfMaps.values()) {
			allLiens.addAll(liensByDestinationSource.values());
		}
		
		return allLiens;
	}
	
	/**
	 * Retrieves the set of sources of all LienRen.
	 * 
	 * @return set of sources
	 */
	public Set<String> getSourceSet() {
		return mapOfMaps.keySet();
	}

}
