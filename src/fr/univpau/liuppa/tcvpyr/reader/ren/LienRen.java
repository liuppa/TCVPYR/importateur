package fr.univpau.liuppa.tcvpyr.reader.ren;

/**
 * Class representing a link (association) according to RenablLP 2 
 * data model. A LienRen may describe the relationship between any kind 
 * of notice in RenablLP 2 (e.g., Merimme, Palissy or Illustration).
 * 
 * @author André Fonteles
 */
public class LienRen {

	public static final String ILLUSTRATION_SIGNIFICATIVE = "illustration significative";
	public static final String ILLUSTRATION = "illustration";
	
	/**
	 * String containing a reference (id) of the ObjetEtude
	 * in the origin (source) of this directed association.
	 */
	private String sourceRef;

	/**
	 * String containing a reference (id) of the ObjetEtude
	 * in the destination of this directed association.
	 */
	private String destinationRef;
	
	/**
	 * String describing the type of the association.
	 */
	private String type;

	public LienRen() {
		super();
	}

	public String getSourceRef() {
		return sourceRef;
	}

	public void setSourceRef(String sourceRef) {
		this.sourceRef = sourceRef;
	}

	public String getDestinationRef() {
		return destinationRef;
	}

	public void setDestinationRef(String destinationRef) {
		this.destinationRef = destinationRef;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
