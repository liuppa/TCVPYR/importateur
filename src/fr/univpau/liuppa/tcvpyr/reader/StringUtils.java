package fr.univpau.liuppa.tcvpyr.reader;

/**
 * Set of Utility classes to help in the reading {@link String}s
 * 
 * @author André Fonteles
 */
public class StringUtils {

	/**
	 * Remove any XML or HTML tag from {@link String} s
	 * 
	 * @param s - String for which the tags will be stripped
	 * @return clean string
	 */
	public static String stripTags(String s) {
		return s.replaceAll("<[^>]+>", "");
	}
	
	/**
	 * Returns a copy of {@link String} s with a capital letter
	 * at the first position.
	 * 
	 * @param s - String to change first letter
	 * @return string with a capital first letter
	 */
	public static String firstLetterToUpperCase(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}
}
