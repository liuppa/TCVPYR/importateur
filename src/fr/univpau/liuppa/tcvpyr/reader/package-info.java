/**
 * Provides classes and interfaces related to reading data from external sources
 * such as a XML from RenablLP2 and GERTRUDE or a JSON from Elasticsearch.
 */
package fr.univpau.liuppa.tcvpyr.reader;