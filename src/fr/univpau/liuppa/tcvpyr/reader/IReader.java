package fr.univpau.liuppa.tcvpyr.reader;

import java.util.List;

import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;

/**
 * Interface defining an object capable of reading a {@link List} of 
 * {@link ObjetEtude} from a source previously configured.
 * 
 * @author André Fonteles
 */
public interface IReader {

	/**
	 * Read and return all objects of type {@link ObjetEtude} 
	 * from a source previously set (e.g., a path to folder 
	 * containing an export from RenablLP 2 or GERTRUDE).
	 * 
	 * @return list of {@link ObjetEtude} extracted from the source.
	 */
	public List<ObjetEtude> read();
	
}
