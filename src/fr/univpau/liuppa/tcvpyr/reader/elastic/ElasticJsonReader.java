package fr.univpau.liuppa.tcvpyr.reader.elastic;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.geojson.GeoJsonReader;

import fr.univpau.liuppa.tcvpyr.model.Adresse;
import fr.univpau.liuppa.tcvpyr.model.Date;
import fr.univpau.liuppa.tcvpyr.model.Illustration;
import fr.univpau.liuppa.tcvpyr.model.Lien;
import fr.univpau.liuppa.tcvpyr.model.Merimee;
import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;
import fr.univpau.liuppa.tcvpyr.model.Palissy;
import fr.univpau.liuppa.tcvpyr.model.Personne;
import fr.univpau.liuppa.tcvpyr.model.Protection;
import fr.univpau.liuppa.tcvpyr.model.Role;
import fr.univpau.liuppa.tcvpyr.reader.IReader;
import fr.univpau.liuppa.tcvpyr.reader.LienUtils;
import fr.univpau.liuppa.tcvpyr.reader.UnpopulatedObjetEtude;

/**
 * Class responsible for reading a JSON from ElasticSearch 
 * containing a set of {@link ObjetEtude}.
 * 
 * @author André Fonteles
 */
public class ElasticJsonReader implements IReader {

	private HashMap<String, ObjetEtude> oeMap;
	private String jsonString;
	private GeoJsonReader geoJsonReader;
	
	public ElasticJsonReader(String jsonString) {
		this.jsonString = jsonString;
		
		geoJsonReader = new  GeoJsonReader();
	}

	/**
	 * Read the JSON of {@link ObjetEtude} and populates oeMap HashMap
	 * with these notices.
	 */
	public List<ObjetEtude> read() {
		oeMap = new HashMap<String, ObjetEtude>();
		
		JSONObject jo = new JSONObject(jsonString);
		
		jo = jo.getJSONObject("hits");
		JSONArray hits = jo.getJSONArray("hits");
		
		JSONObject objetEtudeJO;
		ObjetEtude oe = null;
		
		for (int i = 0; i < hits.length(); i++) {
			objetEtudeJO = hits.getJSONObject(i).getJSONObject("_source");
			
			String instance = objetEtudeJO.getString("instance");
			
			if(instance.contains("Merimee")) {
				oe = new Merimee();
			} else if(instance.contains("Palissy")) {
				oe = new Palissy();
			} else { // If not Merimee or Palissy, skip this object
				continue;
			}
			
			if(oe != null) {
				try {
					oe.setReference(objetEtudeJO.getString("reference"));
					
					oe.setTitre(objetEtudeJO.getString("titre"));
					oe.setDescription(objetEtudeJO.getString("description"));
					oe.setHistorique(objetEtudeJO.getString("historique"));
					oe.setType(objetEtudeJO.getString("type"));
				
					// Read dates
					if(!objetEtudeJO.isNull("dateSaisie"))
						oe.setDateSaisie(new Date(objetEtudeJO.getString("dateSaisie"), "yyyy-MM-dd"));
					if(!objetEtudeJO.isNull("anneesConstructions")) {
						List<Object> oList = objetEtudeJO.getJSONArray("anneesConstructions").toList();
						for (Object object : oList) {
							oe.addAnneesConstructions((Integer)object);
						}
					}
					if(!objetEtudeJO.isNull("datations")) {
						List<Object> oList = objetEtudeJO.getJSONArray("datations").toList();
						for (Object object : oList) {
							oe.addDatation((String)object);
						}
					}

					readAdresse(objetEtudeJO.getJSONObject("adresse"), oe);

					readIllustrations(objetEtudeJO.getJSONArray("illustrations"), oe);
					readRoles(objetEtudeJO.getJSONArray("personnes"), oe);
					readChercheurs(objetEtudeJO.getJSONArray("chercheurs"), oe);
					readProtections(objetEtudeJO.getJSONArray("protections"), oe);
					
					oeMap.put(oe.getReference(), oe);
				} catch (JSONException | ParseException | com.vividsolutions.jts.io.ParseException e) {
					e.printStackTrace();
				}
			}
		}

		// Read and configure all liens for all objetsEtude
		for (int i = 0; i < hits.length(); i++) {
			objetEtudeJO = hits.getJSONObject(i).getJSONObject("_source");
			readLiens(objetEtudeJO.getJSONArray("liens"));
		}
		
		return new ArrayList<ObjetEtude>(oeMap.values());
	}
	
	/**
	 * Read the JSONArray chercheursJA and populates {@link ObjetEtude} oe
	 * with with a list of 'chercheurs' ({@link Personne}).
	 *
	 * @param chercheursJA - JSONArray of checheurs
	 * @param oe - ObjetEtude to be populated
	 */
	private void readChercheurs(JSONArray chercheursJA, ObjetEtude oe) {
		JSONObject chercheurJO;
		
		for (int i = 0; i < chercheursJA.length(); i++) {
			chercheurJO = chercheursJA.getJSONObject(i);
			
			Personne p = new Personne();
			p.setNom(chercheurJO.getString("nom"));
			p.setPrenom(chercheurJO.getString("prenom"));
			
			oe.addChercheur(p);
		}
	}

	/**
	 * Read the address ({@link Adresse}) in JSONObject adresseJO 
	 * and set it to {@link ObjetEtude} oe
	 * 
	 * @param adresseJO - Address to be read
	 * @param oe - ObjetEtude to be populated
	 * @throws com.vividsolutions.jts.io.ParseException 
	 */
	private void readAdresse(JSONObject adresseJO, ObjetEtude oe) throws com.vividsolutions.jts.io.ParseException {
		Adresse a = new Adresse();
		a.setMunicipalite(adresseJO.getString("municipalite"));
		
		if(!adresseJO.isNull("region"))
			a.setRegion(adresseJO.getString("region"));
		if(!adresseJO.isNull("codePostal"))
			a.setCodePostal(adresseJO.getString("codePostal"));
		if(!adresseJO.isNull("nomVoie"))
			a.setNomVoie(adresseJO.getString("nomVoie"));
		if(!adresseJO.isNull("typeVoie"))
			a.setTypeVoie(adresseJO.getString("typeVoie"));
		if(!adresseJO.isNull("numero"))
			a.setNumero(adresseJO.getString("numero"));

		// Extract 'localisation'
		JSONObject localisationJO = adresseJO.getJSONObject("localisation");
		Geometry geoLoc = geoJsonReader.read(localisationJO.toString());
		a.setLocalisation(geoLoc);
		
		oe.setAdresse(a);
	}

	/**
	 * Read a list of illustrations ({@link Illustration}) from JSONArray illuJA
	 * and set it to {@link ObjetEtude} oe. 
	 * 
	 * @param illuJA - List of illustrations
	 * @param oe - ObjetEtude to be populated
	 */
	private void readIllustrations(JSONArray illuJA, ObjetEtude oe) {
		JSONObject illuJO;
		Illustration illu = null;
		
		for (int i = 0; i < illuJA.length(); i++) {
			illuJO = illuJA.getJSONObject(i);
			
			illu = new Illustration();
			illu.setReference(illuJO.getString("reference"));
			illu.setLegende(illuJO.getString("legende"));
			illu.setAccesImage(illuJO.getString("accesImage"));
			illu.setAccesMiniature(illuJO.getString("accesMiniature"));
			illu.setSignificative(illuJO.getBoolean("significative"));
			
			oe.addIllustration(illu);
		}
	}
	
	/**
	 * Read a list of roles ({@link Role}) from JSONArray rolesJA and
	 * set it to {@link ObjetEtude} oe.
	 * 
	 * @param rolesJA - list of roles
	 * @param oe - ObjetEtude to be populated
	 */
	private void readRoles(JSONArray rolesJA, ObjetEtude oe) {
		JSONObject roleJO;
		Role role = null;
		
		for (int i = 0; i < rolesJA.length(); i++) {
			roleJO = rolesJA.getJSONObject(i);
			
			role = new Role();
			role.setObjetEtude(oe);
			role.setDescription(roleJO.getString("role"));

			Personne p = new Personne();
			p.setNom(roleJO.getString("nom"));
			p.setPrenom(roleJO.getString("prenom"));
			
			role.setPersonne(p);
			
			oe.addRolePersonne(role);
		}
	}
	
	/**
	 * Read a list of protections ({@link Protection}) from JSONArray protectionsJA
	 * and set it to {@link ObjetEtude} oe.
	 * 
	 * @param protectionsJA - list of protections
	 * @param oe - ObjetEtude to be populated
	 * 
	 * @throws JSONException
	 * @throws ParseException
	 */
	private void readProtections(JSONArray protectionsJA, ObjetEtude oe) throws JSONException, ParseException {
		JSONObject protectionJO;
		Protection protection = null;
		
		for (int i = 0; i < protectionsJA.length(); i++) {
			protectionJO = protectionsJA.getJSONObject(i);
			
			protection = new Protection();
			protection.setNature(protectionJO.getString("nature"));
			if(protectionJO.has("date")) {
				protection.setDate(new Date(protectionJO.getString("date"), "yyyy-MM-dd"));
			}
			oe.addProtection(protection);
		}
	}
	
	/**
	 * Read and configure all existing links ({@link Lien}) between 
	 * two {@link ObjetEtude}. The references within a ObjetEtude are
	 * also updated. 
	 * 
	 * @param liensJA - list of links ({@link Lien})
	 */
	private void readLiens(JSONArray liensJA) {
		JSONObject lienJO;
		Lien lien = null;
		
		ObjetEtude destination;
		ObjetEtude source;
		for (int i = 0; i < liensJA.length(); i++) {
			lienJO = liensJA.getJSONObject(i);
			
			destination = oeMap.get(lienJO.getString("destination"));
			if(destination == null) {
				destination = new UnpopulatedObjetEtude(lienJO.getString("destination"));
			}
			
			source = oeMap.get(lienJO.getString("source"));
			if(source == null) {
				source = new UnpopulatedObjetEtude(lienJO.getString("source"));
			}
			
			lien = new Lien();
			lien.setDestination(destination);
			lien.setSource(source);
			lien.setType(lienJO.getString("type"));
				
			// If the lien hasn't been already set, configure references
			if(!LienUtils.lienAlreadySet(lien))  {
				destination.addLien(lien);
				destination.addLienDe(lien);
				
				source.addLien(lien);
				source.addLienVers(lien);
			}
		}
	}
}
