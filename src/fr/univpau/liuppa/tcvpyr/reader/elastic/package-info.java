/**
 * Provides one or more classes related to reading JSON data from 
 * Elasticsearch and transforming it into the project data model.
 */
package fr.univpau.liuppa.tcvpyr.reader.elastic;