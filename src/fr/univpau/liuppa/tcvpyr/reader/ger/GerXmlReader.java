package fr.univpau.liuppa.tcvpyr.reader.ger;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.geojson.GeoJsonReader;

import fr.univpau.liuppa.tcvpyr.model.Adresse;
import fr.univpau.liuppa.tcvpyr.model.Date;
import fr.univpau.liuppa.tcvpyr.model.Illustration;
import fr.univpau.liuppa.tcvpyr.model.Lien;
import fr.univpau.liuppa.tcvpyr.model.Merimee;
import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;
import fr.univpau.liuppa.tcvpyr.model.Palissy;
import fr.univpau.liuppa.tcvpyr.model.Personne;
import fr.univpau.liuppa.tcvpyr.model.Protection;
import fr.univpau.liuppa.tcvpyr.model.Role;
import fr.univpau.liuppa.tcvpyr.reader.GeometryUtils;
import fr.univpau.liuppa.tcvpyr.reader.IReader;
import fr.univpau.liuppa.tcvpyr.reader.LienUtils;
import fr.univpau.liuppa.tcvpyr.reader.StringUtils;
import jdk.nashorn.api.scripting.JSObject;

/**
 * Class responsible for reading XML files exported by GERTRUDE
 * contained in a folder.
 * 
 * @author André Fonteles
 */
public class GerXmlReader implements IReader {

	private final DocumentBuilderFactory factory;
	private final DocumentBuilder builder;
	private final XPath xPath;
	private final String rootPath;
	
	private GeoJsonReader geoJsonReader;
	
	private HashMap<String, ObjetEtude> oeMap = null;

	public GerXmlReader(String rootDir) throws ParserConfigurationException {
		factory = DocumentBuilderFactory.newInstance();
		builder = factory.newDocumentBuilder();
		xPath = XPathFactory.newInstance().newXPath();
		
		this.rootPath = rootDir;

		oeMap = new HashMap<String, ObjetEtude>();
		geoJsonReader = new GeoJsonReader();
	}

	/* (non-Javadoc)
	 * @see fr.univpau.iutbayonne.tcvpyr.reader.IReader#read()
	 */
	public List<ObjetEtude> read() {
		File rootDir = new File(rootPath + "/AllEntities.xml");
		System.out.println("[" + new java.util.Date() + "] GerXmlReader: parsing " + rootDir.getAbsolutePath());
		
		Element eGertrudeItem = null;
		try {
			Document document = builder.parse(rootDir);
			eGertrudeItem = document.getDocumentElement();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(eGertrudeItem == null) {
			throw new RuntimeException("Failure to parse file");
		}
		
		try {
			NodeList dossierNL = (NodeList) xPath.evaluate("/*/com.atolcd.gertrude.model.dossier.oeuvre.objet.DossierOeuvreObjet|/*/com.atolcd.gertrude.model.dossier.oeuvre.architecture.DossierOeuvreArchitecture",
						eGertrudeItem, XPathConstants.NODESET);

			ObjetEtude oe;
			for(int i = 0; i < dossierNL.getLength(); i++) {
				try {
					oe = readObjetEtude((Element)dossierNL.item(i));
					oeMap.put(oe.getReference(), oe);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			// Read and build links ("Lien") between objects (ObjetEtude)
			readLiens(eGertrudeItem);
			
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		System.out.println("[" + new java.util.Date() + "] GerXmlreader: converting coordinates from Lambert 2 extended to WGS84");
		convertCoordinates();

		return new ArrayList<ObjetEtude>(oeMap.values());
	}

	/**
	 * Convert coordinates of all ObjetEtude from Lambert 2 extended to WGS84
	 */
	private void convertCoordinates() {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");

		InputStream is = GerXmlReader.class.getClassLoader().getResourceAsStream("assets/proj4-src.js");
		InputStreamReader isReader = new InputStreamReader(is, StandardCharsets.UTF_8);

		// Read Proj4JS script file (Javascript API to convert coordinates)
		BufferedReader bfr = new BufferedReader(isReader);
		
		try {
			engine.eval(bfr);
			engine.eval("proj4.defs('EPSG:27572','+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=2200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs');");
			engine.eval("proj4.defs('EPSG:2154', '+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs ');");

			// Define a 'class' in Javascript to represent a point.
			engine.eval("function Point(x, y) { this.x = x; this.y  = y}");
			Invocable inv = (Invocable) engine;
			
			JSObject pointFunc;
			JSObject pointObj;
			JSObject oArray;
			for(ObjetEtude oe : oeMap.values()) {
			    for(Coordinate c : oe.getAdresse().getLocalisation().getCoordinates()) {
			    	pointFunc = (JSObject)engine.get("Point");
			    	pointObj =  (JSObject)pointFunc.newObject(c.x, c.y);

			    	// Call function from script file to convert (transform) coordinates 
			    	oArray = (JSObject) inv.invokeFunction("proj4", "EPSG:2154", "WGS84", pointObj);
			    	
			    	c.x = Double.parseDouble(oArray.getMember("x").toString());
			    	c.y = Double.parseDouble(oArray.getMember("y").toString());
			    }
			}

		} catch (ScriptException | NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Extract the {@link ObjetEtude} from Element eDossier.
	 * 
	 * @param eDossier - XML element representing an ObjetEtude.
	 * @return objetEtude extracted from eDossier
	 * 
	 * @throws XPathExpressionException
	 * @throws com.vividsolutions.jts.io.ParseException 
	 */
	private ObjetEtude readObjetEtude(Element eDossier) throws XPathExpressionException, com.vividsolutions.jts.io.ParseException {
		ObjetEtude oe = readInstance(eDossier);
		
		// Extract reference
		Element eRef = (Element) eDossier.getElementsByTagName("id").item(0);
		oe.setReference(eRef.getTextContent().trim());
		
		System.out.println("Parsing dossier : " + oe.getReference());
		
		// titre
		Element eTitre = (Element) eDossier.getElementsByTagName("titre").item(0);
		oe.setTitre(eTitre.getTextContent().trim());

		// description
		String description = readDescription(eDossier);
		oe.setDescription(description);
		
		// historique
		String historique = readHistorique(eDossier);
		oe.setHistorique(historique);

		// type
		String type = readType(eDossier);
		oe.setType(type);
		
		// anneesConstruction
		Element eHistorique = (Element) eDossier.getElementsByTagName("historique").item(0);
		NodeList anneeConstrNL = eHistorique.getElementsByTagName("com.atolcd.gertrude.model.historique.DateHistorique");
		Element eDate;
		int annee;
		for(int i = 0; i < anneeConstrNL.getLength(); i++) {
			eDate = (Element) anneeConstrNL.item(i);
			annee = Integer.parseInt(eDate.getElementsByTagName("date").item(0).getTextContent().trim());
			oe.addAnneesConstructions(annee);
		}
		
		// datations
		NodeList datationNL = eHistorique.getElementsByTagName("com.atolcd.gertrude.model.historique.Datation");
		Element eDatation;
		Element eDatationLib;
		for(int i = 0; i < datationNL.getLength(); i++) {
			eDatation = (Element) datationNL.item(i);
			eDatationLib = (Element)eDatation.getElementsByTagName("libelle").item(0);
			if(eDatationLib != null)
				oe.addDatation(eDatationLib.getTextContent().trim());
		}
		
		// dateSaisie
		try {
			Element eDateVersement = (Element) eDossier.getElementsByTagName("dateVersement").item(0);
			String dateString = eDateVersement.getTextContent().substring(0, 10);
			oe.setDateSaisie(new Date(dateString, "yyyy-MM-dd"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		// adresse
		readAdresse(eDossier, oe);
		
		// protection
		readProtections(eDossier, oe);
		
		// Chercheurs
		readChercheurs(eDossier, oe);
		
		// Chercheurs
		readIllustrations(eDossier, oe);
		
		// rolePersonnes
		readRoles(eDossier, oe);
		
		return oe;
	}
	
	/**
	 * Extract the "adresse" ({@link Adresse}) of the ObjetEtude described 
	 * in eDossier and set it to oe.
	 * 
	 * @param eDossier - Element containing the address of oe.
	 * @param oe - ObjetEtude to be populated
	 * 
	 * @throws XPathExpressionException 
	 * @throws com.vividsolutions.jts.io.ParseException 
	 */
	private void readAdresse(Element eDossier, ObjetEtude oe) throws XPathExpressionException, com.vividsolutions.jts.io.ParseException {
		Adresse a = new Adresse();
		
		Element element;
		
		element = (Element) eDossier.getElementsByTagName("geoReferenceId").item(0);
		if(element == null) {
			throw new RuntimeException("Skiping ObjetEtude. Reason: no geoReferenceId");
		}
		String geoReferenceId = element.getTextContent();
		
		// Commune
		Element eLocPrincipal = (Element) eDossier.getElementsByTagName("localisationPrincipale").item(0);
		element = (Element) eLocPrincipal.getElementsByTagName("commune").item(0);
		element = (Element) element.getElementsByTagName("libelle").item(0);
		a.setMunicipalite(element.getTextContent().trim());
		
		element = (Element) eLocPrincipal.getElementsByTagName("region").item(0);
		element = (Element) element.getElementsByTagName("libelle").item(0);
		String region = element.getTextContent().trim();
		if(region.equalsIgnoreCase("Midi-Pyrénées"))
			region = "Occitanie";
		if(region.equalsIgnoreCase("Aquitaine") || region.equalsIgnoreCase("Limousin") || region.equalsIgnoreCase("Poitou-Charentes"))
			a.setRegion("Nouvelle-Aquitaine");
		
		
		// Extract numero, typeVoie, nomVoie form first "adresse" element only
		Element eAdresse = (Element) eLocPrincipal.getElementsByTagName("com.atolcd.gertrude.model.localisation.Adresse").item(0);

		if(eAdresse != null) {
			// numero
			element = (Element) eAdresse.getElementsByTagName("numero").item(0);
			if(element != null) {
				a.setNumero(element.getTextContent().trim());
			}
	
			// typeVoie
			element = (Element) eAdresse.getElementsByTagName("typeVoie").item(0);
			if(element != null) {
				element = (Element) element.getElementsByTagName("libelle").item(0);
			}
			
			// nomVoie
			element = (Element) eAdresse.getElementsByTagName("nom").item(0);
			if(element != null) {
				element = (Element) element.getElementsByTagName("libelle").item(0);
				if(element != null) {
					a.setNomVoie(element.getTextContent().trim());
				}
			}
		}
		
		// Localisation
		Element eGeometry = (Element)xPath.evaluate("/*/com.atolcd.gertrude.model.cartographie.GeoReference[id[text()='" + geoReferenceId + "']]/geometry",
				eDossier, XPathConstants.NODE);
		String geoJson = eGeometry.getTextContent();
		
		Geometry geoLoc = geoJsonReader.read(geoJson);
		
		// Validade and set the geometry as the "localisation"
		a.setLocalisation(GeometryUtils.validate(geoLoc));
		
		oe.setAdresse(a);
	}

	/**
	 * Extract type of the ObjetEtude described in eDossier.
	 * 
	 * @param eDossier - XML Element containing the information to be read 
	 * @return type
	 */
	private String readType(Element eDossier) {
		Element eHelper = (Element) eDossier.getElementsByTagName("designation").item(0);
		eHelper = (Element) eHelper.getElementsByTagName("denominations").item(0);
		NodeList denoLibelleNL = eHelper.getElementsByTagName("libelle");
		
		String type = "";

		Element eDenoLibelle;
		for (int i = 0; i < denoLibelleNL.getLength(); i++) {
			eDenoLibelle = (Element)denoLibelleNL.item(i);
			type += eDenoLibelle.getTextContent().trim() + ", ";
		}
		if(denoLibelleNL.getLength() > 0) // Check if necessary to remove the last: ", ".
			type = type.substring(0, type.length()-2); 
		
		return type;
	}

	/**
	 * Extract historique of the ObjetEtude described in eDossier.
	 * 
	 * @param eDossier - XML Element containing the information to be read
	 * @return historique
	 */
	private String readHistorique(Element eDossier) {
		Element eHistorique = (Element) eDossier.getElementsByTagName("historique").item(0);
		eHistorique = (Element) eHistorique.getElementsByTagName("historique").item(0);
		
		String historique = "";
		if(eHistorique != null) {
			historique = eHistorique.getTextContent().trim();
			historique = StringUtils.stripTags(historique);
		}
		
		return historique;
	}

	/**
	 * Extract description of the ObjetEtude described in eDossier.
	 * 
	 * @param eDossier - XML Element containing the information to be read
	 * @return description
	 */
	private String readDescription(Element eDossier) {
		String description = "";

		Element eDescription = (Element) eDossier.getElementsByTagName("description").item(0);
		if(eDescription != null) {
			Element ePrecisionRep = (Element) eDescription.getElementsByTagName("precisionRepresentation").item(0);
			eDescription = (Element) eDescription.getElementsByTagName("description").item(0);
			
			if (eDescription != null && eDescription.getTextContent().length() > 0) {
				description = eDescription.getTextContent();
			}
			if (ePrecisionRep != null && ePrecisionRep.getTextContent().length() > 0) {
				description += " " + ePrecisionRep.getTextContent();
			}
			
			description = StringUtils.stripTags(description.trim());
		}
		
		return description;
	}

	/**
	 * Instantiate a concrete class of {@link ObjetEtude} according to 
	 * {@link Element} eDossier.
	 * 
	 * @param eDossier - XML Element containing the information to be read
	 * @return instance of {@link ObjetEtude} according to eDossier
	 */
	private ObjetEtude readInstance(Element eDossier) {
		ObjetEtude oe = null;
		
		if(eDossier.getTagName().contains("DossierOeuvreObjet")) {
			oe = new Palissy();
		} else if (eDossier.getTagName().contains("DossierOeuvreArchitecture")) {
			oe = new Merimee();
		} else {
			throw new RuntimeException("Unsupported element " + eDossier.getTagName() + " for an ObjetEtude.");
		}

		return oe;
	}

	/**
	 * Extract all peoples ({@link Personne}) and their roles from element eGertrudeItem.
	 * Add all roles ({@link Role}) to {@link ObjetEtude} oe.
	 * 
	 * @param eGertrudeItem - XML Element containing the information to be read
	 * @param oe - ObjetEtude to be populated
	 * 
	 * @throws XPathExpressionException
	 */
	private void readRoles(Element eGertrudeItem, ObjetEtude oe) throws XPathExpressionException {
		NodeList professionsNL = (NodeList)xPath.evaluate("/*/*[id[text()='" + oe.getReference() + "']]/historique/professions/com.atolcd.gertrude.model.auteur.Profession",
				eGertrudeItem, XPathConstants.NODESET);
		
		Element eProf;
		Personne p;
		Role r;
		
		for (int i = 0; i < professionsNL.getLength(); i++) {
			eProf = (Element)professionsNL.item(i);
			Element eAuteurId = (Element) eProf.getElementsByTagName("auteurId").item(0);
			NodeList tLibellesNL = eProf.getElementsByTagName("com.atolcd.gertrude.model.thesaurus.ThesaurusField");
			
			Element ePersonne = (Element)xPath.evaluate("/*/com.atolcd.gertrude.model.auteur.Personne[id[text()='" + eAuteurId.getTextContent() + "']]",
					eGertrudeItem, XPathConstants.NODE);
			
			p = parsePersonne(ePersonne);
			r = new Role();
			r.setPersonne(p);
			
			String descriptionR = "";
			for(int j = 0; j < tLibellesNL.getLength(); j++) {
				descriptionR += ((Element)((Element)tLibellesNL.item(j)).getElementsByTagName("libelle").item(0)).getTextContent().trim() + " ";
			}
			descriptionR = descriptionR.trim();
			
			r.setDescription(descriptionR);
			r.setObjetEtude(oe);
			oe.addRolePersonne(r);
		}
	}
	
	/**
	 * Extract all researchers ({@link Personne}) linked with {@link ObjetEtude} oe 
	 * and add them to {@link ObjetEtude} oe.
	 * 
	 * @param eGertrudeItem - XML Element containing the information to be read
	 * @param oe - ObjetEtude to be populated
	 * 
	 * @throws XPathExpressionException
	 */
	private void readChercheurs(Element eGertrudeItem, ObjetEtude oe) throws XPathExpressionException {
		NodeList contribDossierNL = (NodeList)xPath.evaluate("/*/*[id[text()='" + oe.getReference() + "']]/etude/contributions/com.atolcd.gertrude.model.dossier.ContributionDossier",
				eGertrudeItem, XPathConstants.NODESET);
		
		Element eContribDossier;
		for (int i = 0; i < contribDossierNL.getLength(); i++) {
			eContribDossier = (Element)contribDossierNL.item(i);
			Element eAuteurId = (Element) eContribDossier.getElementsByTagName("auteurId").item(0);
			
			Element ePersonne = (Element)xPath.evaluate("/*/com.atolcd.gertrude.model.auteur.Personne[id[text()='" + eAuteurId.getTextContent() + "']]",
					eGertrudeItem, XPathConstants.NODE);
			
			oe.addChercheur(parsePersonne(ePersonne));
		}
	}
	
	/**
	 * Extract all illustration ({@link Illustration}) linked with {@link ObjetEtude} oe 
	 * and set them to oe.
	 * 
	 * @param eGertrudeItem - XML Element containing the information to be read
	 * @param oe - ObjetEtude to be populated
	 * 
	 * @throws XPathExpressionException
	 */
	private void readIllustrations(Element eGertrudeItem, ObjetEtude oe) throws XPathExpressionException {
		NodeList legendesNL = (NodeList)xPath.evaluate("/*/*[id[text()='" + oe.getReference() + "']]/legendes/com.atolcd.gertrude.model.illustration.Legende",
				eGertrudeItem, XPathConstants.NODESET);
		
		Element eLegende;
		Illustration illu;
		
		for (int i = 0; i < legendesNL.getLength(); i++) {
			eLegende = (Element)legendesNL.item(i);
			Element eIlluId = (Element) eLegende.getElementsByTagName("illustrationId").item(0);
			Element eLegText = (Element) eLegende.getElementsByTagName("legende").item(0);
			Element eSigText = (Element) eLegende.getElementsByTagName("significatif").item(0);
			boolean significatif = false;
			if(eSigText != null)
				significatif = Boolean.parseBoolean(eSigText.getTextContent().trim());
			
			Element eIllustration = (Element)xPath.evaluate("/*/com.atolcd.gertrude.model.illustration.Illustration[id[text()='" + eIlluId.getTextContent() + "']]",
					eGertrudeItem, XPathConstants.NODE);
			
			
			Element eMini = (Element) eIllustration.getElementsByTagName("miniatureResourceId").item(0);
			Element ePlein = (Element) eIllustration.getElementsByTagName("pleinEcranResourceId").item(0);
			
			// If the illustration has no files (i.e., empty "fichiers" tag) attached to it, skip it.
			if(eMini != null || ePlein != null) {
				// If either miniature or full-sized image does not exist,
				// copy the existing one to the non-existing one.
				if(eMini == null)
					eMini = ePlein;
				if(ePlein == null)
					ePlein = eMini;
				
				illu = new Illustration();
				illu.setReference(eIlluId.getTextContent().trim());
				illu.setLegende(eLegText.getTextContent().trim());
				illu.setAccesMiniature(eMini.getTextContent());
				illu.setAccesImage(ePlein.getTextContent());
				illu.setSignificative(significatif);
				
				oe.addIllustration(illu);
			}
		}
	}
	
	/**
	 * Extract all protections ({@link Protection}) linked with {@link ObjetEtude} oe 
	 * according to element eGertrudeItem and set them to oe.
	 * 
	 * @param eGertrudeItem - XML Element containing the information to be read
	 * @param oe - ObjetEtude to be populated
	 * @throws XPathExpressionException
	 */
	private void readProtections(Element eGertrudeItem, ObjetEtude oe) throws XPathExpressionException {
		NodeList protectionsNL = (NodeList)xPath.evaluate("/*/*[id[text()='" + oe.getReference() + "']]/statutJuridique/protections/com.atolcd.gertrude.model.statutprotection.Protection",
				eGertrudeItem, XPathConstants.NODESET);
		
		Element eProtection;
		Protection protection;
		
		for (int i = 0; i < protectionsNL.getLength(); i++) {
			eProtection = (Element)protectionsNL.item(i);
			
			Element eNatLib = (Element) eProtection.getElementsByTagName("libelle").item(0);
			Element eDate = (Element) eProtection.getElementsByTagName("string").item(0);
			
			protection = new Protection();
			
			if(eNatLib!= null) {
				protection.setNature(eNatLib.getTextContent().trim());
			}
			
			if(eDate!= null) {
				String dateString = eDate.getTextContent().trim();
			
				try {
					Date d = new Date(dateString, "yyyy/MM/dd");
					protection.setDate(d);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			oe.addProtection(protection);
		}
	}
	
	/**
	 * Extract and configure all "liens" ({@link Lien}) from all 
	 * {@link ObjetEtude} according to element eGertrudeItem. All
	 * references liens of all ObjetEtude in variable oeMap are 
	 * updated.
	 * 
	 * @param eGertrudeItem - XML Element containing the information to be read
	 * 
	 * @throws XPathExpressionException
	 */
	private void readLiens(Element eGertrudeItem) throws XPathExpressionException {
		
		Element eLien;
		Element eSourceId;
		Element eSousDossierId;
		ObjetEtude source;
		ObjetEtude destination;
		Lien lien;
		String type;

		NodeList lienNL = (NodeList)xPath.evaluate("/*/com.atolcd.gertrude.model.lien.LienPartieConstituante|/*/com.atolcd.gertrude.model.lien.LienEdifice",
				eGertrudeItem, XPathConstants.NODESET);
		
		// Iterate over all liens
		for (int i = 0; i < lienNL.getLength(); i++) {
			eLien = (Element)lienNL.item(i);
			
			// Extract the reference of the source
			if(eLien.getTagName().contains(Lien.LIEN_PARTIE_CONSTITUANTE)) {
				eSourceId = (Element) eLien.getElementsByTagName("dossierParentId").item(0);
				type = Lien.LIEN_PARTIE_CONSTITUANTE;
			} else {
				eSourceId = (Element) eLien.getElementsByTagName("edificeId").item(0);
				type = Lien.LIEN_EDIFICE;
			}
			
			// Extract the reference of the destination
			eSousDossierId = (Element) eLien.getElementsByTagName("sousDossierId").item(0);

			// Find source and destination according to their references
			source = oeMap.get(eSourceId.getTextContent().trim());
			destination = oeMap.get(eSousDossierId.getTextContent().trim());
			
			// If source and destination exist create a lien.
			if(source != null && destination != null) {
				lien = new Lien();
				lien.setSource(source);
				lien.setDestination(destination);
				lien.setType(type);
				
				// Configure a lien only if it hasn't been already set
				if(!LienUtils.lienAlreadySet(lien))  {
					destination.addLien(lien);
					destination.addLienDe(lien);
					
					source.addLien(lien);
					source.addLienVers(lien);
				}
			}
		}
	}
	
	/**
	 * Extract a {@link Personne} from element ePersonne 
	 * 
	 * @param ePersonne - XML Element containing the information to be read
	 * @return Personne extracted from ePersonne
	 */
	private Personne parsePersonne(Element ePersonne) {
		Element eNom = (Element) ePersonne.getElementsByTagName("nom").item(0);
		Element ePrenom = (Element) ePersonne.getElementsByTagName("prenom").item(0);

		Personne p = new Personne();
		if(eNom != null)
			p.setNom(eNom.getTextContent().trim());
		else
			p.setNom("");
		if(ePrenom != null)
			p.setPrenom(ePrenom.getTextContent().trim());
		else
			p.setPrenom("");

		return p;
	}
}
