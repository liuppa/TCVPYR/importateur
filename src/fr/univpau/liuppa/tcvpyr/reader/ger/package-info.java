/**
 * Provides one or more classes related to reading data from a XML 
 * exported by GERTRUDE and transforming it into the project data model.
 */
package fr.univpau.liuppa.tcvpyr.reader.ger;