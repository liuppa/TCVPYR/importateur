package fr.univpau.liuppa.tcvpyr.reader;

import java.util.Collection;
import java.util.Iterator;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.operation.polygonize.Polygonizer;

public class GeometryUtils {
	
	/**
	 * If the geometry contains a polygon or multipolygon, self intersections /
	 * inconsistencies are fixed.
	 * 
	 * @param geometry - geometry to be validated
	 * @return a geometry 
	 */
	public static Geometry validate(Geometry geometry) {
		Geometry[] geometries = new Geometry[geometry.getNumGeometries()];
		
		Geometry g;
		for(int i = 0; i < geometries.length; i++) {
			g = geometry.getGeometryN(i);
			if(g instanceof GeometryCollection) {
				g = validate(g); // Calls this function recursively if it is a GeometryCollection
			} else {
				g = validatePolygon(g); // Validates it in case it is a polygon or multipolygon
			}
			
			geometries[i] = g;
		}
		GeometryFactory factory = new GeometryFactory();

		return factory.createGeometryCollection(geometries);
	}
	
	/**
	 * Get / create a valid version of the geometry given. If the geometry is a polygon or multi polygon, self intersections /
	 * inconsistencies are fixed. Otherwise the geometry is returned.
	 * 
	 * @param geometry - geometry to be validated
	 * @return a geometry 
	 */
	@SuppressWarnings("unchecked")
	private static Geometry validatePolygon(Geometry geometry){
	    if(geometry instanceof Polygon){
	        if(geometry.isValid()){
	            return geometry; // If the polygon is valid just return it
	        }
	        Polygonizer polygonizer = new Polygonizer();
	        addPolygon((Polygon)geometry, polygonizer);
	        return toPolygonGeometry(polygonizer.getPolygons(), geometry.getFactory());
	    }else if(geometry instanceof MultiPolygon){
	        if(geometry.isValid()){
	            return geometry; // If the multipolygon is valid just return it
	        }
	        Polygonizer polygonizer = new Polygonizer();
	        for(int n = geometry.getNumGeometries(); n-- > 0;){
	            addPolygon((Polygon)geometry.getGeometryN(n), polygonizer);
	        }
	        return toPolygonGeometry(polygonizer.getPolygons(), geometry.getFactory());
	    }else{
	        return geometry; // In my case, I only care about polygon / multipolygon geometries
	    }
	}

	/**
	 * Add all line strings from the polygon given to the polygonizer given
	 * 
	 * @param polygon polygon from which to extract line strings
	 * @param polygonizer polygonizer
	 */
	static void addPolygon(Polygon polygon, Polygonizer polygonizer){
	    addLineString(polygon.getExteriorRing(), polygonizer);
	    for(int n = polygon.getNumInteriorRing(); n-- > 0;){
	        addLineString(polygon.getInteriorRingN(n), polygonizer);
	    }
	}

	/**
	 * Add the linestring given to the polygonizer
	 * 
	 * @param lineString line string
	 * @param polygonizer polygonizer
	 */
	static void addLineString(LineString lineString, Polygonizer polygonizer){

	    if(lineString instanceof LinearRing){ // LinearRings are treated differently to line strings : we need a LineString NOT a LinearRing
	        lineString = lineString.getFactory().createLineString(lineString.getCoordinateSequence());
	    }

	    // unioning the linestring with the point makes any self intersections explicit.
	    Point point = lineString.getFactory().createPoint(lineString.getCoordinateN(0));
	    Geometry toAdd = lineString.union(point); 

	    //Add result to polygonizer
	    polygonizer.add(toAdd);
	}

	/**
	 * Get a geometry from a collection of polygons.
	 * 
	 * @param polygons collection
	 * @param factory factory to generate MultiPolygon if required
	 * @return null if there were no polygons, the polygon if there was only one, or a MultiPolygon containing all polygons otherwise
	 */
	static Geometry toPolygonGeometry(Collection<Polygon> polygons, GeometryFactory factory){
	    switch(polygons.size()){
	        case 0:
	            return null; // No valid polygons!
	        case 1:
	            return polygons.iterator().next(); // single polygon - no need to wrap
	        default:
	            //polygons may still overlap! Need to sym difference them
	            Iterator<Polygon> iter = polygons.iterator();
	            Geometry ret = iter.next();
	            while(iter.hasNext()){
	                ret = ret.symDifference(iter.next());
	            }
	            return ret;
	    }
	}
}
