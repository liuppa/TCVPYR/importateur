package fr.univpau.liuppa.tcvpyr.reader;

import fr.univpau.liuppa.tcvpyr.model.Date;

/**
 * Set of Utility classes to help in the reading of a {@link Date}
 * 
 * @author André Fonteles
 */
public class DateUtils {

	
	/**
	 * Parse a string in one of the following formats to date:
	 * - YYYY 		(e.g., "1999/12")
	 * - MM/YYYY	(e.g., "1999")
	 * - dd/MM/YYYY	(e.g., "1999/12/20")
	 * 
	 * When no month is provided, the date is set to January
	 * When no day is provided, the date is set to 1st day of the month. 
	 * 
	 * @param dateString - String containing the date
	 * @return date
	 */
	public static Date parse(String dateString) {
		String units[] = dateString.trim().split("/");

		int year = Integer.parseInt(units[0]);
		int month = (units.length >= 2) ? Integer.parseInt(units[1]) : -1;
		int day = (units.length >= 3) ? Integer.parseInt(units[2]) : -1;

		Date d = new Date();
		d.setDay(day);
		d.setMonth(month);
		d.setYear(year);

		return d;
	}
	
}
