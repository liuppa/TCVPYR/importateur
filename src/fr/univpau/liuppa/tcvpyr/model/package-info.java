/**
 * Package containing all classes related to the the data model of the project.
 */
package fr.univpau.liuppa.tcvpyr.model;