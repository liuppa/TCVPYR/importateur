package fr.univpau.liuppa.tcvpyr.model;

/**
 * Class representing an {@link ObjetEtude} of kind "mobilier" (furniture, house objects, etc) 
 * 
 * @author André Fonteles
 */
public class Palissy extends ObjetEtude {

	public Palissy() {
		super();
	}
	
}
