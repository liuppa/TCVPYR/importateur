package fr.univpau.liuppa.tcvpyr.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class representing a research subject. A research subject
 * can be instantiated as one of its concrete subclasses, including: 
 * {@link Merimee}, {@link Palissy} and {@link Pci}.
 * 
 * @author André Fonteles
 */
public abstract class ObjetEtude {

	private String reference;
	private String titre;
	private String description;
	private String historique;
	private String type;
	private Date dateSaisie;
	private String bdOrigine;

	private List<Integer> anneesConstructions;
	private List<String> datations;
	
	private Adresse adresse;
	
	private List<Personne> chercheurs;
	private List<Role> rolesPersonnes;
	private List<Protection> protections;
	private List<Illustration> illustrations;
	private List<Lien> liens;
	private List<Lien> liensDe;
	private List<Lien> liensVers;

	public ObjetEtude() {
		liens = new ArrayList<Lien>();
		liensDe = new ArrayList<Lien>();
		liensVers = new ArrayList<Lien>();
		chercheurs = new ArrayList<Personne>();
		rolesPersonnes = new ArrayList<Role>();
		protections = new ArrayList<Protection>();
		illustrations = new ArrayList<Illustration>();
		anneesConstructions = new ArrayList<Integer>();
		datations = new ArrayList<String>();
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHistorique() {
		return historique;
	}

	public void setHistorique(String historique) {
		this.historique = historique;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Integer> getAnneesConstructions() {
		return anneesConstructions;
	}

	public void addAnneesConstructions(int anneeConstruction) {
		anneesConstructions.add(anneeConstruction);
	}

	public List<String> getDatations() {
		return datations;
	}

	public void addDatation(String datation) {
		datations.add(datation);
	}

	public Date getDateSaisie() {
		return dateSaisie;
	}

	public void setDateSaisie(Date dateSaisie) {
		this.dateSaisie = dateSaisie;
	}

	public List<Lien> getLiens() {
		return liens;
	}

	public void addLien(Lien lien) {
		liens.add(lien);
	}
	
	public List<Lien> getLiensDe() {
		return liensDe;
	}

	public void addLienDe(Lien lien) {
		liensDe.add(lien);
	}
	
	public List<Lien> getLiensVers() {
		return liensVers;
	}

	public void addLienVers(Lien lien) {
		liensVers.add(lien);
	}
	
	public void addIllustration(Illustration illustration) {
		illustrations.add(illustration);
	}
	
	public void addIllustrations(List<Illustration> illustrations) {
		this.illustrations.addAll(illustrations);
	}
	
	public List<Illustration> getIllustrations() {
		return illustrations;
	}
	
	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public List<Protection> getProtections() {
		return protections;
	}
	
	public void addProtection(Protection p) {
		protections.add(p);
	}

	public List<Role> getRolesPersonnes() {
		return rolesPersonnes;
	}
	
	public void addRolePersonne(Role role) {
		rolesPersonnes.add(role);
	}

	public void addRolesPersonnes(List<Role> roles) {
		rolesPersonnes.addAll(roles);
	}

	public List<Personne> getChercheurs() {
		return chercheurs;
	}

	public void addChercheur(Personne chercheur) {
		chercheurs.add(chercheur);
	}

	public String getBdOrigine() {
		return bdOrigine;
	}

	public void setBdOrigine(String bdOrigine) {
		this.bdOrigine = bdOrigine;
	}

	/**
	 * Return the first person linked to this {@link ObjetEtude} 
	 * by a whole containing the String word
	 * 
	 * @param word - word to be found in the description of the role
	 * @return personne whose role contains the String word or null if none is found
	 */
	public Personne getPersonneWhoseRoleContains(String word) {
		word = word.toLowerCase();
		
		for (Role r : rolesPersonnes) {
			if(r.getDescription().toLowerCase().indexOf(word) >= 0)
				return r.getPersonne();
		}
		return null;
	}
	
	/**
	 * Verify if this ObjetEtude is a historical monument 
	 * of regional relevance (in French, "inscrit au titre de momuments 
	 * historiques")
	 * 
	 * @return true if it is and false otherwise
	 */
	public boolean inscriptionAuTitreMH() {
		return getProtectionWhoseNatureContains("inscrit MH") != null;
	}
	
	/**
	 * Verify if this ObjetEtude is a historical monument 
	 * of national relevance (in French, "classé au titre de momuments 
	 * historiques")
	 * 
	 * @return true if it is and false otherwise
	 */
	public boolean classementAuTitreMH() {
		return getProtectionWhoseNatureContains("classé MH") != null;
	}
	
	/**
	 * Return the first protection of this ObjetEtude 
	 * in which the nature contains the String word
	 * 
	 * @param word - word to be found in the nature of the protection
	 * @return protection in which nature contains the String word or null if none is found
	 */
	public Protection getProtectionWhoseNatureContains(String word) {
		for (Protection p : protections) {
			if(p.getNature().equalsIgnoreCase(word)) {
				return p;
			}
		}
		return null;
	}
	
	/**
	 * Attempt to find a representative illustration of the
	 * ObjetEtude. If no illustration exists, return null.
	 * 
	 * @return a representative illustration if possible and null if no illustration exists
	 */
	public Illustration getIllustrationSignificative() {
		Illustration i = null;
		if(illustrations != null) {
			for (Illustration illustration: illustrations) {
				if(illustration.isSignificative()) {
					i = illustration;
					break;
				}
			}

			if(i == null && illustrations.size() > 0) {
				i = illustrations.get(0);
			}
		}
		
		return i;
	}
}
