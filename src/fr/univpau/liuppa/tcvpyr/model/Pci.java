package fr.univpau.liuppa.tcvpyr.model;

/**
 * Class representing an {@link ObjetEtude} of kind "Patrimoine Culturel Immatériel" (Intangible Cultural Heritage).
 * A Pci may represent practices, expression, knowledge, skills and etc. of some people. 
 * 
 * @author André Fonteles
 */
public class Pci extends ObjetEtude {

	public Pci() {
		super();
	}
	
}
