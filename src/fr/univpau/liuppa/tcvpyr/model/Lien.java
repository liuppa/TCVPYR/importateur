package fr.univpau.liuppa.tcvpyr.model;

/**
 * Class representing a link between two {@link ObjetEtude}. 
 * 
 * Note that although this link (Lien) can be created from a link 
 * between two "notices" in RenablLP 2, they are not equivalent. 
 * A link in RenablLP 2 may bind an {@link ObjetEtude} and a {@link Illustration}
 * while this link can't.
 * 
 * @author André Fonteles
 */
public class Lien {

	public static final String LIEN_EDIFICE = "LienEdifice";
	public static final String LIEN_PARTIE_CONSTITUANTE = "LienPartieConstituante";
	
	private ObjetEtude source;
	private ObjetEtude destination;
	private String type;

	public Lien() {
		super();
	}
	
	public Lien(ObjetEtude source, ObjetEtude destination, String type) {
		super();
		this.source = source;
		this.destination = destination;
		this.type = type;
	}

	public ObjetEtude getSource() {
		return source;
	}

	public void setSource(ObjetEtude source) {
		this.source = source;
	}

	public ObjetEtude getDestination() {
		return destination;
	}

	public void setDestination(ObjetEtude destination) {
		this.destination = destination;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
