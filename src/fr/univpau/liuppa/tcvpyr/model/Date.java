package fr.univpau.liuppa.tcvpyr.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Class representing a date in terms of year, month and day of the month.
 * If any of these three are not informed, they are set to -1. This behavior 
 * allows awareness of what parts of the date have been informed.
 * 
 * @author André Fonteles
 */
public class Date {
	private int year;
	private int month;
	private int day;
	
	public Date() {
		year = -1;
		month = -1;
		day = -1;
	}

	public Date(String dateString, String format) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Calendar c = GregorianCalendar.getInstance();
		c.clear();
		
		c.setTime(sdf.parse(dateString));
		
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH) + 1;
		day = c.get(Calendar.DAY_OF_MONTH);
	}
	
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}
	
	public String toString(String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Calendar c = GregorianCalendar.getInstance();
		c.clear();

		c.set(Calendar.YEAR, year);
		
		if(month > 0)
			c.set(Calendar.MONTH, month-1);
		else
			c.set(Calendar.MONTH, Calendar.JANUARY);
		
		if(day > 0)
			c.set(Calendar.DAY_OF_MONTH, day);
		else
			c.set(Calendar.DAY_OF_MONTH, 1);
			
		return sdf.format(c.getTime());
	}
}
