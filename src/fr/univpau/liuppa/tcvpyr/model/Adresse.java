package fr.univpau.liuppa.tcvpyr.model;

import com.vividsolutions.jts.geom.Geometry;

/**
 * This class represents an address of an {@link ObjetEtude}.
 * 
 * @author André Fonteles
 */
public class Adresse {

	private String municipalite;
	private String region;
	private String numero;
	private String typeVoie;
	private String nomVoie;
	private String codePostal;

	private Geometry localisation;

	public Adresse() {
	}

	public String getMunicipalite() {
		return municipalite;
	}

	public void setMunicipalite(String municipalite) {
		this.municipalite = municipalite;
	}
	
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTypeVoie() {
		return typeVoie;
	}

	public void setTypeVoie(String typeVoie) {
		this.typeVoie = typeVoie;
	}

	public String getNomVoie() {
		return nomVoie;
	}

	public void setNomVoie(String nomVoie) {
		this.nomVoie = nomVoie;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public Geometry getLocalisation() {
		return localisation;
	}

	public void setLocalisation(Geometry localisation) {
		this.localisation = localisation;
	}
	
	public String getNumeroTypeVoieNomVoie() {
		if(typeVoie == null || nomVoie == null)
			return null;

		String adresse = "";
		
		if(numero != null)
			adresse += numero + " ";
		adresse = typeVoie + " " + nomVoie;
		
		return adresse;
	}
}
