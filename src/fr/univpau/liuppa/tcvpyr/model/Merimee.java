package fr.univpau.liuppa.tcvpyr.model;

/**
 * Class representing an {@link ObjetEtude} of kind "bâtiments" (building) 
 * 
 * @author André Fonteles
 */
public class Merimee extends ObjetEtude {

	public Merimee() {
		super();
	}
}
