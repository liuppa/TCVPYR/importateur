package fr.univpau.liuppa.tcvpyr.model;

/**
 * Class representing a protection of an {@link ObjetEtude} 
 * (e.g., "monument historique").
 * 
 * @author André Fonteles
 */
public class Protection {

	private String nature;
	private Date date;

	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
