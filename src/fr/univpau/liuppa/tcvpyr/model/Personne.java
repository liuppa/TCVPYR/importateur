package fr.univpau.liuppa.tcvpyr.model;

/**
 * Class modeling a person. This class can be used to 
 * represent a chercheur (researcher) of an {@link ObjetEtude}
 * or any person having an important role related to
 * an {@link ObjetEtude}.
 * 
 * @author André Fonteles
 */
public class Personne {

	private String prenom;
	private String nom;

	public Personne() {
	}
	
	public Personne(String prenom, String nom) {
		super();
		this.prenom = prenom;
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
