package fr.univpau.liuppa.tcvpyr.model;

/**
 * This class models an illustration of an {@link ObjetEtude}.
 * 
 * @author André Fonteles
 */
public class Illustration {
	
	private String reference;
	private String accesMiniature;
	private String accesImage;
	private String legende;
	
	private boolean significative;
	
	public Illustration() {
		super();
	}
	
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getAccesMiniature() {
		return accesMiniature;
	}

	public void setAccesMiniature(String accesMiniature) {
		this.accesMiniature = accesMiniature;
	}

	public String getAccesImage() {
		return accesImage;
	}

	public void setAccesImage(String accesImage) {
		this.accesImage = accesImage;
	}

	public String getLegende() {
		return legende;
	}

	public void setLegende(String legende) {
		this.legende = legende;
	}

	public boolean isSignificative() {
		return significative;
	}

	public void setSignificative(boolean significative) {
		this.significative = significative;
	}
	
}
