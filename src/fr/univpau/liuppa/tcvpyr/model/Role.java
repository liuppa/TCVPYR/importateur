package fr.univpau.liuppa.tcvpyr.model;

/**
 * Class describing what role a {@link Personne} (person) 
 * has played related to {@link ObjetEtude}.
 * 
 * @author André Foneles
 */
public class Role {

	public static final String TYPE_ARCHITECTE = "architecte";
	public static final String TYPE_PEINTRE = "peintre";
	public static final String TYPE_COMMANDITAIRE = "commanditaire";
	
	private ObjetEtude objetEtude;
	private Personne personne;
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Role() {
		super();
	}

	public Role(ObjetEtude objetDEtude, Personne personne) {
		super();
		this.objetEtude = objetDEtude;
		this.personne = personne;
	}

	public ObjetEtude getObjetEtude() {
		return objetEtude;
	}

	public void setObjetEtude(ObjetEtude objetEtude) {
		this.objetEtude = objetEtude;
	}

	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

}
