package fr.univpau.liuppa.tcvpyr.writer.mapsxml;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.univpau.liuppa.tcvpyr.model.Illustration;
import fr.univpau.liuppa.tcvpyr.model.Merimee;
import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;
import fr.univpau.liuppa.tcvpyr.model.Palissy;

public class MapsXmlWriter {

	private final DocumentBuilderFactory factory;
	private final DocumentBuilder builder;
	private String filePath;

	private List<ObjetEtude> oeList = null;

	public MapsXmlWriter(List<ObjetEtude> oeList, String filePath) throws ParserConfigurationException {
		super();

		factory = DocumentBuilderFactory.newInstance();
		builder = factory.newDocumentBuilder();

		this.oeList = oeList;
		this.filePath = filePath;
	}

	/**
	 * Write the content of merimeesList, palissysList and illustrationsList into a
	 * XML file defined by filePath.
	 * 
	 * @throws TransformerException 
	 */
	public void write() throws TransformerException {
		Document doc = parseToXmlDoc();
		
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(filePath));

		transformer.transform(source, result);
	}
	
	/**
	 * Parse the list of {@link ObjetEtude}s previously set to
	 * a xml {@link Document}.
	 * 
	 * @return document created from previously set list of {@link ObjetEtude}s
	 */
	public Document parseToXmlDoc() {
		Document doc = builder.newDocument();

		// Create root element
		Element rootElement = doc.createElement("markers");
		doc.appendChild(rootElement);

		for (ObjetEtude oe : oeList) {
			// Create marker
			Element eMarker = doc.createElement("marker");
			rootElement.appendChild(eMarker);

			// Configure marker parameters
			Attr attr = doc.createAttribute("type");
			
			if(oe instanceof Merimee)
				attr.setValue("Merimee");
			else if (oe instanceof Palissy)
				attr.setValue("Palissy");
			else
				throw new RuntimeException("Parsing ObjetEtude of type " + oe.getClass() + " not implemented");
			
			eMarker.setAttributeNode(attr);
			
			addObjetEtudeInfo(oe, eMarker, doc);
		}
		
		return doc;
	}

	/**
	 * Add information from ObjetEtude into the XML marker tag
	 * @param objetEtude - ObjetEtude from which information will be added
	 * @param eMarker - xml element where the information will be added
	 * @param doc - Document used to create XML attributes and elements
	 */
	private void addObjetEtudeInfo(ObjetEtude objetEtude, Element eMarker, Document doc) {
		// Configure marker parameters
		Attr attr = doc.createAttribute("ref");
		attr.setValue(objetEtude.getReference());
		eMarker.setAttributeNode(attr);
		
		// Create knownAs
		Element eKnownAs = doc.createElement("knownAs");
		eMarker.appendChild(eKnownAs);
		eKnownAs.appendChild(doc.createTextNode(objetEtude.getTitre()));
		
		// TODO : Decide behavior when polyline, polygon ...
		attr = doc.createAttribute("lat");
		attr.setValue(objetEtude.getAdresse().getLocalisation().getCoordinates()[0].y + "");
		eMarker.setAttributeNode(attr);
		attr = doc.createAttribute("lng");
		attr.setValue(objetEtude.getAdresse().getLocalisation().getCoordinates()[0].x + "");
		eMarker.setAttributeNode(attr);
		
		// Create description
		Element eDescription = doc.createElement("description");
		eMarker.appendChild(eDescription);
		
		if(objetEtude.getHistorique() != null && objetEtude.getHistorique().length() > 0)
			eDescription.appendChild(doc.createTextNode(objetEtude.getHistorique()));
		else // If historique is empty, use description.
			eDescription.appendChild(doc.createTextNode(objetEtude.getDescription()));
		
		// Create description
		Element eThumbnail = doc.createElement("thumbnail");
		eMarker.appendChild(eThumbnail);
		
		// Add illustration
		Illustration i = objetEtude.getIllustrationSignificative();
		
		String imagePath = "";
		if(i != null) {
			imagePath = i.getAccesMiniature();
		}
		
		eThumbnail.appendChild(doc.createTextNode(imagePath));
	}
}
