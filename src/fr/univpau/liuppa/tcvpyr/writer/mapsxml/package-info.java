/**
 * Provides one or more classes related to writing data from our 
 * data model into a XML understandable by our Google Maps project
 * (GeoInventaire).
 */
package fr.univpau.liuppa.tcvpyr.writer.mapsxml;