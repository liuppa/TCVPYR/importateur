package fr.univpau.liuppa.tcvpyr.writer.elastic;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.json.JSONArray;
import org.json.JSONObject;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.geojson.GeoJsonWriter;

import fr.univpau.liuppa.tcvpyr.model.Adresse;
import fr.univpau.liuppa.tcvpyr.model.Illustration;
import fr.univpau.liuppa.tcvpyr.model.Lien;
import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;
import fr.univpau.liuppa.tcvpyr.model.Personne;
import fr.univpau.liuppa.tcvpyr.model.Protection;
import fr.univpau.liuppa.tcvpyr.model.Role;

/**
 * Class responsible for writing on Elastisearch
 * a list of ObjetEtude.
 * 
 * @author André Fonteles
 */
public class ElasticWriter {
	private String host;
	private String elasticIndex;

	private GeoJsonWriter geoJsonWriter;
	
	public ElasticWriter(String host, String elasticIndex) {
		this.host = host;
		this.elasticIndex = elasticIndex;
		
		geoJsonWriter = new GeoJsonWriter();
	}
	
	/**
	 * Write to the ElasticSearch database the oeList of 
	 * {@link ObjetEtude}.
	 * 
	 * @param oeList - List of {@link ObjetEtude} to be written
	 */
	public void write(List<ObjetEtude> oeList) {
		
		try {
			ensureMapping();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		StringBuffer sb = new StringBuffer("");
		
		// Create JSON query
		for (ObjetEtude oe : oeList) {
			sb.append("{\"index\":{\"_id\":\"").append(oe.getReference()).append("\", ");
			sb.append("\"_index\":\"").append(elasticIndex).append("\", ");
			sb.append("\"_type\":\"_doc\"}}");
			sb.append("\n");
			sb.append(objetEtudeToJSON(oe).toString());
			sb.append("\n");
		}
		
		HttpEntity entity = new NStringEntity(sb.toString(), ContentType.APPLICATION_JSON);
		RestClient restClient = RestClient.builder(new HttpHost(host, 9200, "http")).build();

		Response response;
		try {
			System.out.println("[" + new Date() + "] ElasticWriter: writing into Elasticsearch.");
			
			response = restClient.performRequest("POST", "_bulk", Collections.emptyMap(), entity);
			InputStream inputStream = response.getEntity().getContent();
			Scanner s = new Scanner(inputStream);
			
			System.out.print("[" + new Date() + "] ElasticWriter: Elasticsearch response :");
			while(s.hasNext()) {
				System.out.print(" " + s.nextLine());
			}
			System.out.println();
			
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				restClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void ensureMapping() throws IOException {
		RestClient restClient = null;
		
		try {
			restClient = RestClient.builder(new HttpHost(host, 9200, "http")).build();
			
			// Query Elasticsearch for a json containing the list of ObjetEtudes
			Response resp = restClient.performRequest("HEAD", elasticIndex);
			
			// If the index does not exist, create it
			if(resp.getStatusLine().getStatusCode() != 200) {
				// Read mapping request
				InputStream is = ElasticWriter.class.getClassLoader().getResourceAsStream("assets/es/mp_objet_etude.txt");
				InputStreamReader isReader = new InputStreamReader(is, StandardCharsets.UTF_8);
				Scanner s = new Scanner(isReader);
				
				String mappingQuery = "";
				while(s.hasNext()) {
					mappingQuery += s.nextLine();
				}
				s.close();
				
				HttpEntity entity = new NStringEntity(mappingQuery, ContentType.APPLICATION_JSON);
				resp = restClient.performRequest(
						"PUT", 
						elasticIndex,
						Collections.emptyMap(), entity);
				
				// If the request has failed to create the map
				if(resp.getStatusLine().getStatusCode() != 200) {
					throw new IOException(); 
				}
			}
		} finally {
			if(restClient != null)
				restClient.close();
		}
	}
	
	/**
	 * Parse an {@link ObjetEtude} to a JSON.
	 * 
	 * @param oe - ObjetEtude to be parsed to JSON
	 * @return JSONObject created from {@link ObjetEtude}
	 */
	private JSONObject objetEtudeToJSON(ObjetEtude oe) {
		JSONObject jo = new JSONObject();

		jo.put("reference", oe.getReference());
		jo.put("bdOrigine", oe.getBdOrigine());
		jo.put("titre", oe.getTitre());
		jo.put("description", oe.getDescription());
		jo.put("historique", oe.getHistorique());
		jo.put("type", oe.getType());
		jo.put("anneesConstructions", new JSONArray(oe.getAnneesConstructions()));
		jo.put("datations", new JSONArray(oe.getDatations()));
		jo.put("dateSaisie", oe.getDateSaisie().toString("yyyy-MM-dd"));
		jo.put("instance", oe.getClass());
		jo.put("adresse", adresseToJSON(oe.getAdresse()));
		jo.put("illustrations", illustrationsToJSON(oe.getIllustrations()));
		jo.put("chercheurs", chercheursToJSON(oe.getChercheurs()));
		jo.put("personnes", rolesToJSON(oe.getRolesPersonnes()));
		jo.put("protections", protectionsToJSON(oe.getProtections()));
		jo.put("liens", liensToJSON(oe.getLiens()));
		jo.put("liensDe", liensToJSON(oe.getLiensDe()));
		jo.put("liensVers", liensToJSON(oe.getLiensVers()));
		
		return jo;
	}

	/**
	 * Parse a list of "chercheurs" ({@link Personne}) to a JSON.
	 * 
	 * @param chercheurs - list of chercheurs to be parsed
	 * @return JSONArray created from the list of 'chercheurs'
	 */
	private JSONArray chercheursToJSON(List<Personne> chercheurs) {
		JSONArray ja = new JSONArray();
		JSONObject jo;
		for (Personne p : chercheurs) {
			jo = new JSONObject();
			jo.put("nom", p.getNom());
			jo.put("prenom", p.getPrenom());
			
			ja.put(jo);
		}
		return ja;
	}

	/**
	 * Parse a list of roles ({@link Role}) to a JSON.
	 * 
	 * @param roles - list of roles to be parsed
	 * @return JSONArray created from the list of roles.
	 */
	private JSONArray rolesToJSON(List<Role> roles) {
		JSONArray ja = new JSONArray();
		for (Role r : roles) {
			ja.put(roleToJSON(r));
		}
		return ja;
	}
	
	/**
	 * Parse a {@link Role} to a JSONObject.
	 * 
	 * @param r - role to be parsed
	 * @return JSONObject created from {@link Role} r
	 */
	private JSONObject roleToJSON(Role r) {
		JSONObject jo = new JSONObject();
		jo.put("nom", r.getPersonne().getNom());
		jo.put("prenom", r.getPersonne().getPrenom());
		jo.put("role", r.getDescription());
		
		return jo;
	}
	
	/**
	 * Parse a list of protections ({@link Protection}) to a JSON.
	 * 
	 * @param protections - list of protections to be parsed
	 * @return JSONArray created from the list of protections
	 */
	private JSONArray protectionsToJSON(List<Protection> protections) {
		JSONArray ja = new JSONArray();
		for (Protection p : protections) {
			ja.put(protectionToJSON(p));
		}
		return ja;
	}
	
	/**
	 * Parse a {@link Protection} to a JSON.
	 * 
	 * @param p - Protection to be parsed
	 * @return JSONObject created from {@link Protection} p
	 */
	private JSONObject protectionToJSON(Protection p) {
		JSONObject jo = new JSONObject();
		jo.put("nature", p.getNature());
		if(p.getDate() != null)
			jo.put("date", p.getDate().toString("yyyy-MM-dd"));

		return jo;
	}
	
	/**
	 * Parse a list of "liens" ({@link Lien}) to a JSON.
	 * 
	 * @param liens - list of liens do be parsed
	 * @return JSONArray created from the list of liens
	 */
	private JSONArray liensToJSON(List<Lien> liens) {
		JSONArray ja = new JSONArray();
		for (Lien l : liens) {
			ja.put(lienToJSON(l));
		}
		return ja;
	}
	
	/**
	 * Parse a {@link Lien} to a JSON.
	 * 
	 * @param l - lien to be parsed
	 * @return JSONObject created from {@link Lien} l
	 */
	private JSONObject lienToJSON(Lien l) {
		JSONObject jo = new JSONObject();
		jo.put("type", l.getType());
		jo.put("source", l.getSource().getReference());
		jo.put("destination", l.getDestination().getReference());

		return jo;
	}
	
	/**
	 * Parse a list of illustrations ({@link Illustration}) to a JSON.
	 * 
	 * @param illustrations - list of illustration to be parsed
	 * @return JSONArray created from the list of illustrations
	 */
	private JSONArray illustrationsToJSON(List<Illustration> illustrations) {
		JSONArray ja = new JSONArray();
		for (Illustration i : illustrations) {
			ja.put(illustrationToJSON(i));
		}
		return ja;
	}

	/**
	 * Parse an {@link Illustration} to a JSON.
	 * 
	 * @param i - illustration to be parsed
	 * @return JSONObject created from {@link Illustration} i
	 */
	private JSONObject illustrationToJSON(Illustration i) {
		JSONObject jo = new JSONObject();
		jo.put("reference", i.getReference());
		jo.put("accesImage", i.getAccesImage());
		jo.put("accesMiniature", i.getAccesMiniature());
		jo.put("legende", i.getLegende());
		jo.put("significative", i.isSignificative());

		return jo;
	}
	
	/**
	 * Parse an {@link Adresse} to a JSON.
	 * 
	 * @param a - "adresse" to be passed
	 * @return JSONobject created from {@link Adresse} a
	 */
	private JSONObject adresseToJSON(Adresse a) {
		JSONObject jo = new JSONObject();
		jo.put("codePostal", a.getCodePostal());
		jo.put("municipalite", a.getMunicipalite());
		jo.put("region", a.getRegion());
		jo.put("nomVoie", a.getNomVoie());
		jo.put("typeVoie", a.getTypeVoie());
		jo.put("numero", a.getNumero());
		
		Geometry geoLoc = a.getLocalisation();
		JSONObject joLocation = new JSONObject(geoJsonWriter.write(geoLoc));
		jo.put("localisation", joLocation);
		
		return jo;
	}
}
