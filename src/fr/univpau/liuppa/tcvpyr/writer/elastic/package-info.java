/**
 * Provides one or more classes related to writing data from our 
 * data model into Elasticsearch
 */
package fr.univpau.liuppa.tcvpyr.writer.elastic;