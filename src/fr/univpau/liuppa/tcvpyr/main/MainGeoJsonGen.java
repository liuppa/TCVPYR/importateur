package fr.univpau.liuppa.tcvpyr.main;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import org.apache.http.HttpHost;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;

import com.vividsolutions.jts.io.geojson.GeoJsonWriter;

import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;
import fr.univpau.liuppa.tcvpyr.reader.elastic.ElasticJsonReader;

public class MainGeoJsonGen {

	public static void main(String[] args) throws IOException {
		RestClient restClient = RestClient.builder(new HttpHost("localhost", 9200, "http")).build();

		try {
			Response elasticResp = restClient.performRequest(
					"GET", 
					"objetetude/_search?size=10000",
					Collections.emptyMap());

			InputStream inputStream = elasticResp.getEntity().getContent();

			String json = "";
			Scanner s2 = new Scanner(inputStream);
			while (s2.hasNext()) {
				json += s2.nextLine();
			}
			s2.close();

			ElasticJsonReader eReader = new ElasticJsonReader(json);
			List<ObjetEtude> oeList = eReader.read();
			
			String geoJson = "{\"type\":\"FeatureCollection\",\"features\": [\n";
			
			GeoJsonWriter geoJsonWriter = new GeoJsonWriter();
			for (ObjetEtude objetEtude : oeList) {
				geoJson += "{\"type\":\"Feature\",\"geometry\":" + geoJsonWriter.write(objetEtude.getAdresse().getLocalisation()) + "},";
			}
			
			geoJson = geoJson.substring(0, geoJson.length()-1) + "]}";
			
			PrintWriter out = new PrintWriter("geojson.json");
			out.println(geoJson);
			out.flush();
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			restClient.close();
		}
	}
	
}
