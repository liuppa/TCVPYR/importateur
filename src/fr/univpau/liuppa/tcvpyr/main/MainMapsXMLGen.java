package fr.univpau.liuppa.tcvpyr.main;

import java.io.File;
import java.util.List;

import javax.swing.JFileChooser;

import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;
import fr.univpau.liuppa.tcvpyr.reader.ren.RenXmlReader;
import fr.univpau.liuppa.tcvpyr.writer.mapsxml.MapsXmlWriter;

/**
 * Entry point of a program that creates a XML file directly 
 * from a export for the Google Maps project used to view 
 * ObjetEtude on a map.
 * 
 * @author André Fonteles
 */
public class MainMapsXMLGen {

	public static void main(String[] args) {
		try {
			JFileChooser fc = new JFileChooser("C:\\Users\\TCVPYR\\Documents\\TCVPYR\\Exports base de données\\Export Renable\\export bagnères");
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = fc.showOpenDialog(null);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				RenXmlReader renXmlParser = new RenXmlReader(file.getAbsolutePath());

				System.out.println("Opening: " + file.getName());
				List<ObjetEtude> oeList = renXmlParser.read();
				System.out.println("End");

				System.out.println("OE " + oeList.size());

				MapsXmlWriter mapsXmlWriter = new MapsXmlWriter(
						oeList,
						fc.getSelectedFile().getAbsolutePath() + "/output.xml");
				
				mapsXmlWriter.write();

			} else {
				System.out.println("Open command cancelled by user.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
