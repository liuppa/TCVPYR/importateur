package fr.univpau.liuppa.tcvpyr.main;

import javax.xml.parsers.ParserConfigurationException;

import fr.univpau.liuppa.tcvpyr.Importateur;
import fr.univpau.liuppa.tcvpyr.Importateur.ImportOrigin;

/**
 * Entry point of a program used to import data from
 * GERTRUDE or RenablLP2 XML into the Elasticsearch database
 * using the command line.
 * 
 * @author André Fonteles
 *
 */
public class MainCommandLine {

	/**
	 * This program should be executed as stated below, following the exact same order of parameters. <br>
	 * <pre>
	 * java -jar Importateur [imported_files_origin] [imported_files_path] [elasticsearch_url]
	 * </pre>
	 * For example:
	 * <pre>
	 * java -jar Importateur gertrude /home/gertrude/estaing localhost
	 * </pre>
	 * @param args - arguments for the execution of the program where args[0]=[imported_files_origin], 
	 * args[1]=[imported_files_path] and args[2]=[elasticsearch_url]
	 */
	public static void main(String[] args) {
		ImportOrigin importOrigin = getImportOrigin(args);
		String path = getPath(args);
		String urlElasticsearch = getUrlElasticsearch(args);
		boolean production = isProduction(args);
		
		if(args.length >= 1) {
			if(args[0].equalsIgnoreCase("GERTRUDE")) {
				importOrigin = ImportOrigin.GERTRUDE;
			} else if(args[0].equalsIgnoreCase("RENABLLP2")) { 
				importOrigin = ImportOrigin.RENABL_LP2;
			} else {
				throw new RuntimeException("Invalid argument: origin of the data being imported is not known");
			}
		} else {
			throw new RuntimeException("Missing argument: origin of the data being imported");
		}
		
		// Treats the second argument : path to the files to be imported
		if(args.length >= 2) {
			if(args[1].isEmpty()) {
				throw new RuntimeException("Invalid argument: the path is not valid");
			}
			path = args[1];
		} else {
			throw new RuntimeException("Missing argument: path to the files to be imported");
		}
		
		// Treats the third argument : Elasticsearch url
		if(args.length >= 3) {
			if(args[2].isEmpty()) {
				throw new RuntimeException("Invalid argument: the Elasticsearch url is not valid");
			}
			urlElasticsearch = args[2];
		} else {
			throw new RuntimeException("Missing argument: Elasticsearch url");
		}
		
		try {
			Importateur importateur = new Importateur(
					importOrigin, // Define whether importing from GERTRUDE or RenablLP2
					path, // Path containing the files with info. to be imported
					urlElasticsearch, // Elasticsearch url
					production); // Whether importing to production or not

			importateur.importData();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Treats the first argument : origin of the data being imported (e.g., RenablLP2, Gertrude, ...)
	 * @param args - list of arguments
	 * @return the origin of the files being imported. Throw an exception if the origin is invalid
	 */
	private static ImportOrigin getImportOrigin(String[] args) {
		if(args.length >= 1) {
			if(args[0].equalsIgnoreCase("GERTRUDE")) {
				return ImportOrigin.GERTRUDE;
			} else if(args[0].equalsIgnoreCase("RENABLLP2")) { 
				return ImportOrigin.RENABL_LP2;
			}
		}
		
		throw new RuntimeException("Missing or invalid argument: origin of the data being imported");
	}
	
	/**
	 * Treats the second argument : path to the files to be imported
	 * @param args - list of arguments
	 * @return the path to the files being imported. Throw an exception if the path is invalid
	 */
	private static String getPath(String[] args) {
		if(args.length >= 2 && !args[1].isEmpty())
			return args[1];

		throw new RuntimeException("Missing or invalid argument: path to the files to be imported");
	}

	/**
	 * Treats the third argument : Elasticsearch url
	 * @param args - list of arguments
	 * @return url to the Elasticsearch server. Throw an exception if the url is invalid
	 */
	private static String getUrlElasticsearch(String[] args) {
		if(args.length >= 3 && !args[2].isEmpty())
			return args[2];

		throw new RuntimeException("Missing or invalid argument: Elasticsearch url");
	}
	
	/**
	 * Treats the forth argument : environment to where data is being imported
	 * @param args - list of arguments
	 * @return true if importing to production, false otherwise
	 */
	private static boolean isProduction(String[] args) {
		if(args.length >= 4 && !args[1].isEmpty())
			return args[1].equalsIgnoreCase("production");

		throw new RuntimeException("Missing or invalid argument: environment to where data is being imported");
	}
}
