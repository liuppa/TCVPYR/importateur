/**
 * Provides the classes which are entry points of the program. All classes containing
 * main methods are here.
 */
package fr.univpau.liuppa.tcvpyr.main;