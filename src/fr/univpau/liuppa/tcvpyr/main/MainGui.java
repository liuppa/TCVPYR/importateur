package fr.univpau.liuppa.tcvpyr.main;

import java.io.File;

import javax.swing.JFileChooser;
import javax.xml.parsers.ParserConfigurationException;

import fr.univpau.liuppa.tcvpyr.Importateur;
import fr.univpau.liuppa.tcvpyr.Importateur.ImportOrigin;

/**
 * Entry point of a program used to import data from
 * GERTRUDE or RenablLP2 XML into the Elasticsearch database
 * using a GUI.
 * 
 * @author André Fonteles
 *
 */
public class MainGui {

	public static void main(String[] args) {
		try {
			JFileChooser fc = new JFileChooser("C:\\Users\\TCVPYR\\Documents\\TCVPYR\\Exports base de données");
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = fc.showOpenDialog(null);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();

				Importateur importateur = new Importateur(
						ImportOrigin.GERTRUDE, // Define whether importing from GERTRUDE or RenablLP2
						file.getAbsolutePath(), // Path containing the files with info. to be imported
						"localhost", // Elasticsearch url
						true); // Whether importing to production or not 

				importateur.importData();
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

}
