#!/bin/sh

ORIGIN=$1
DESTINATION=$2

mkdir -p "$DESTINATION"

rsync -av -r --progress "$ORIGIN/Images/" "$DESTINATION/" --exclude=*.txt --exclude=*.xml
